<?php


Route::group(["middleware"=>['role:teacher|admin', "web","auth"],"prefix" => 'bo/prof'], function(){

    Route::post("/course/savecourse", "BackOffice\Admin\CourseAdminController@savecourse")
    ->name("admin.course.savecourse");

    Route::get("/course/list", "BackOffice\CourseController@list")
    ->name("admin.course.list");

    Route::get("/course/add", "BackOffice\CourseController@add")
        ->name("admin.course.add");

    Route::get("/course/delete/{id}", "BackOffice\CourseController@delete")
        ->name("admin.course.delete");

    Route::post("/course/delete_image", "BackOffice\CourseController@delete_image")
        ->name("admin.course.delete_image");

    Route::get("/course/updateForm/{id}", "BackOffice\CourseController@updateForm")
        ->name("admin.course.updateForm");

    Route::post("/course/updatecourse", "BackOffice\CourseController@updatecourse")
        ->name("admin.course.updatecourse");

    Route::post('/assets/save', "BackOffice\AssetsController@save")
        ->name("admin.assets.save");

});



/**
* ####################-ALL NEED AUTHENTICATION-###########################################
*/
Route::group(["middleware" => ["auth","web","role:admin|teacher|superadmin"],"prefix"=>"bo"], function(){

    /** --------------------------COURSE--------------------------------------- **/

    Route::get('/course/list', 'BackOffice\CourseController@index')
        ->name('bo.prof.course.list');

    Route::get('/cours/create', 'BackOffice\CourseController@create')
        ->name('bo.prof.course.new');

    Route::post('/cours/store', 'BackOffice\CourseController@store')
        ->name('bo.prof.course.store');

    Route::post('/cours/update/{id}', 'BackOffice\CourseController@update')
        ->name('bo.prof.course.update');

    Route::get('/cours/edit/{id}', 'BackOffice\CourseController@edit')
        ->name('bo.prof.course.edit');

    Route::get("/course/{id}/destroy", "BackOffice\CourseController@destroy")
        ->name('bo.prof.course.destroy');

});


Route::get('/course', 'CourseController@public_view')
    ->name('public.course.list');

Route::get('/course/course_details/{id_course}', 'CourseController@course_details')
    ->name('public.course.details');

Route::get('/cours/list', 'BackOffice\CourseController@public_list')
    ->name('public.cours.list');


?>
