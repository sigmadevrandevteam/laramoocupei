<?php

/**
* All user access that has the prof, administrator  role
*
*/


/**
* Anyone authenticated
*
*/
Route::group(["middleware"=>["web","auth"],"prefix" => 'authenticated'], function(){

    Route::get("/user/profile/{id}", "BackOffice\ProfileController@index")
        ->name("user_profile");

});


?>
