<?php

Route::group(["middleware" => ['role:admin','web','auth'], 'prefix' => 'admin'], function() {

    /************************** COURSE ******************************************************/

    Route::get("/course/manager", "BackOffice\Admin\CourseAdminController@manager")
        ->name("bo.admin.manager");

    Route::get('/teachers/courses', 'BackOffice\Admin\CourseAdminController@index')
        ->name('bo.admin.course.index');

    Route::get('/teachers/courses/actions/{id}', 'BackOffice\Admin\CourseAdminController@actions')
        ->name('bo.admin.course.actions');

    Route::post('/teachers/courses/assign', 'BackOffice\Admin\CourseAdminController@save_assignment')
        ->name('bo.admin.course.save_assignment');

    Route::post('/teachers/courses/rupture', 'BackOffice\Admin\CourseAdminController@rupture_assignation')
        ->name('bo.admin.course.rupture_assignation');

    Route::post("/teachers/show/{id}", 'BackOffice\Admin\TeacherAdminController@ajax_teacher')
        ->name('bo.admin.teacher.showId');


});

?>
