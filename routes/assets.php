<?php


Route::group(["middleware"=>['role:teacher|admin', "web","auth"],"prefix" => 'bo/prof'],
function(){

    Route::get('/users', "BackOffice\UsersController@index")
        ->name("superadmin.users.index");

    Route::get('/assets/typeList', "BackOffice\AssetsController@type_list")
    ->name("superadmin.assets.typeList");

    Route::post('/assets/addtype', "BackOffice\AssetsController@addtype")
    ->name("superadmin.assets.addtype");

    Route::get('/assets/deletetype/{id}', "BackOffice\AssetsController@deletetype")
    ->name("superadmin.assets.deletetype");

    Route::get('/assets/list', "BackOffice\AssetsController@list")
    ->name("superadmin.assets.list");


});

/**
* Anyone authenticated
*
*/
Route::group(["middleware"=>["web","auth"],"prefix" => 'authenticated'], function(){

    Route::post("upload_file_cropped", "ImageController@upload_file_cropped")
        ->name('upload_file_cropped');

    Route::get("auth_forum_index","BackOffice\ForumController@index")
        ->name('auth.forum.index');
});



Route::get("cropper_profile", "ImageController@cropper")
    ->name('cropper_profile');




?>
