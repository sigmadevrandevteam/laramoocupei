<?php

/***************************************************************************************/

/**
* ####################-ALL NEED AUTHENTICATION-###########################################
*/
Route::group(["middleware" => ["auth","web","role:admin|teacher|superadmin"],"prefix"=>"bo"], function(){

    Route::get('get_list_user_role/{id}',"BackOffice\RolesController@UserHasRoles")
        ->name('get_list_user_role');

    /** --------------------------COURSE--------------------------------------- **/

    Route::get('/course/list', 'BackOffice\CourseController@index')
        ->name('bo.prof.course.list');

    Route::get('/cours/create', 'BackOffice\CourseController@create')
        ->name('bo.prof.course.new');

    Route::post('/cours/store', 'BackOffice\CourseController@store')
        ->name('bo.prof.course.store');

    Route::post('/cours/update/{id}', 'BackOffice\CourseController@update')
        ->name('bo.prof.course.update');

    Route::get('/cours/edit/{id}', 'BackOffice\CourseController@edit')
        ->name('bo.prof.course.edit');

    Route::get("/course/{id}/destroy", "BackOffice\CourseController@destroy")
        ->name('bo.prof.course.destroy');


});
/**
*
* All user access that has the superadministrator role
*
*/
Route::group(["middleware"=>['role:superadmin',"web","auth"],"prefix" => 'super-admin'], function(){


});


Route::group(["middleware" => ['role:admin','web','auth'], 'prefix' => 'admin'], function() {

    /*************************** The User managment ********************************/

    Route::get('/', "BackOffice\Admin\UsersController@index")
        ->name("admin.users.index");

    Route::get('/users', "BackOffice\Admin\UsersController@index")
        ->name("admin.users.index");

    Route::post("/user/create", "BackOffice\Admin\UsersController@create")->name('admin.user.create');

    /**********************************************************************/
    Route::get("manage/roles", "BackOffice\RolesController@manage")
        ->name("admin.roles.list");

    /************************** COURSE ******************************************************/

    Route::get("/course/manager", "BackOffice\Admin\CourseAdminController@manager")
        ->name("bo.admin.manager");

    Route::get('/teachers/courses', 'BackOffice\Admin\CourseAdminController@index')
        ->name('bo.admin.course.index');

    Route::get('/teachers/courses/actions/{id}', 'BackOffice\Admin\CourseAdminController@actions')
        ->name('bo.admin.course.actions');

    Route::post('/teachers/courses/assign', 'BackOffice\Admin\CourseAdminController@save_assignment')
        ->name('bo.admin.course.save_assignment');

    Route::post('/teachers/courses/rupture', 'BackOffice\Admin\CourseAdminController@rupture_assignation')
        ->name('bo.admin.course.rupture_assignation');

    Route::post("/teachers/show/{id}", 'BackOffice\Admin\TeacherAdminController@ajax_teacher')
        ->name('bo.admin.teacher.showId');
    /** -------------------------MATTER----------------------------------------- **/

    Route::get('/matter/list', 'BackOffice\Admin\MatterAdminController@index')
        ->name('bo.prof.matter.list');

    Route::get('/matter/show/{id}', 'BackOffice\Admin\MatterAdminController@show')
        ->name('bo.prof.matter.show');

    Route::get('/matter/create', 'BackOffice\Admin\MatterAdminController@create')
        ->name('bo.prof.matter.new');

    Route::post('/matter/store', 'BackOffice\Admin\MatterAdminController@store')
        ->name('bo.prof.matter.store');

    Route::get('/matter/edit/{id}', 'BackOffice\Admin\MatterAdminController@edit')
        ->name('bo.prof.matter.edit');

    Route::post('/matter/update/{id}', 'BackOffice\Admin\MatterAdminController@update')
        ->name('bo.prof.matter.update');

    Route::get('/matter/destroy/{id}', 'BackOffice\Admin\MatterAdminController@destroy')
        ->name('bo.prof.matter.destroy');

    Route::get('/matter/actions/{id}', 'BackOffice\Admin\MatterAdminController@actions')
        ->name('bo.prof.matter.actions');

    Route::post('/matter/assigner/{id}', 'BackOffice\Admin\MatterAdminController@assigner')
        ->name('bo.prof.matter.assigner');

    Route::post('/matter/delete/{id}', "BackOffice\Admin\MatterAdminController@delete")
        ->name('bo.prof.matter.delete.confirmed');

    Route::get("/matter/course/destroy/{id}/matter/{matter_id}", "BackOffice\Admin\MatterAdminController@course_destroy")
        ->name('bo.prof.matter.course_destroy');


});
/**
* All user access that has the administrator or superadministrator role
*
*/
Route::group(["middleware"=>["role:admin|superadmin"],"prefix" => 'bo/admin',"web","auth"], function(){

/*********************** Studiant USER *****************************************/



    Route::post("/studiant/add", "BackOffice\StudiantController@store")
    ->name("admin.studiant.add");

    Route::get("user", "BackOffice\StudiantController@index")
        ->name('manager.studiant.user');


    Route::post("/studaint/delete/{id}", "BackOffice\StudiantController@delete")
    ->name("admin.studiant.delete");

    Route::get("/studaint/edit/{id}", "BackOffice\StudiantController@edit")
    ->name("admin.studiant.edit");

    Route::post("/studaint/save_edit/{id}", "BackOffice\StudiantController@save_edit")
    ->name("admin.studiant.save_edit");

    Route::resource('/etudiant', 'etudiant\etudiantController');

    Route::get('student/ass_course/{id}', 'etudiant\etudiantController@coures_list');
    Route::get('/etudiant/assign/{id}/{course_id}', 'etudiant\etudiantController@assigantion');
    Route::get('/etudiant/deassign/{id}/{course_id}', 'etudiant\etudiantController@deassigantion');

/************************* End Studiant ****************************************/


/*************************** End of The User Creation *****************************/

    Route::post("/role/assignment", "BackOffice\RolesController@assignment")
        ->name("admin.role.assignment");

    Route::get("manage/roles", "BackOffice\RolesController@manage")
        ->name("admin.roles.list");

    Route::get("manage/permission", "BackOffice\PermissionController@index")
        ->name("admin.permission.list");

    Route::post("manage/permission/add", "BackOffice\PermissionController@create")
        ->name("admin.permission.add");


    Route::get("/teacher/list", "BackOffice\TeacherController@index")
    ->name("admin.teacher.list");

    Route::post("/teacher/add", "BackOffice\TeacherController@store")
    ->name("admin.teacher.add");

    Route::get("/teacher/edit/{id}", "BackOffice\TeacherController@edit")
    ->name("admin.teacher.edit");


});


/**
* All user access that has the prof, administrator  role
*
*/
Route::group(["middleware"=>['role:teacher|admin', "web","auth"],"prefix" => 'bo/prof'], function(){


    Route::get('/users', "BackOffice\UsersController@index")
        ->name("superadmin.users.index");

    Route::get('/assets/typeList', "BackOffice\AssetsController@type_list")
    ->name("superadmin.assets.typeList");

    Route::post('/assets/addtype', "BackOffice\AssetsController@addtype")
    ->name("superadmin.assets.addtype");

    Route::get('/assets/deletetype/{id}', "BackOffice\AssetsController@deletetype")
    ->name("superadmin.assets.deletetype");

    Route::get('/assets/list', "BackOffice\AssetsController@list")
    ->name("superadmin.assets.list");


    Route::get("/course/list", "BackOffice\CourseController@list")
        ->name("admin.course.list");

    Route::get("/course/add", "BackOffice\CourseController@add")
        ->name("admin.course.add");

    Route::post("/course/savecourse", "BackOffice\CourseController@savecourse")
        ->name("admin.course.savecourse");

    Route::get("/course/delete/{id}", "BackOffice\CourseController@delete")
        ->name("admin.course.delete");

    Route::post("/course/delete_image", "BackOffice\CourseController@delete_image")
        ->name("admin.course.delete_image");

    Route::get("/course/updateForm/{id}", "BackOffice\CourseController@updateForm")
        ->name("admin.course.updateForm");

    Route::post("/course/updatecourse", "BackOffice\CourseController@updatecourse")
        ->name("admin.course.updatecourse");

    Route::post('/assets/save', "BackOffice\AssetsController@save")
        ->name("admin.assets.save");

});

/**
* Anyone authenticated
*
*/
Route::group(["middleware"=>["web","auth"],"prefix" => 'authenticated'], function(){

    Route::get('dashboard', "BackOffice\DashboardController@manager")
        ->name("backOffice.dashboard");

    Route::get("/user/profile/{id}", "BackOffice\ProfileController@index")
        ->name("user_profile");

    Route::post("upload_file_cropped", "ImageController@upload_file_cropped")
        ->name('upload_file_cropped');

    Route::get("auth_forum_index","BackOffice\ForumController@index")
        ->name('auth.forum.index');

});

/**
* ####################-END ALL NEED AUTHENTICATION-###########################################
*/
Auth::routes();

/**
* ####################-ALL DO NOT NEED AUTHENTICATION-####################################
*/

Route::get('/', 'HomeController@index')
    ->name('public.home');

Route::get('/course', 'CourseController@public_view')
    ->name('public.course.list');

Route::get('/course/course_details/{id_course}', 'CourseController@course_details')
    ->name('public.course.details');

Route::get('/news', 'CourseController@news')
    ->name('public.news');

Route::get('/contact', 'ContactController@index')
    ->name('public.contact');

/*******************************************************************/

Route::get('/error404', 'ErrorController@index404')
    ->name('error404');

Route::get('/logout', 'Auth\LoginController@logout')
    ->name('logout');

Route::get("cropper_profile", "ImageController@cropper")
    ->name('cropper_profile');

Route::get('/cours/list', 'BackOffice\CourseController@public_list')
    ->name('public.cours.list');


/**
* ####################-END ALL DO NOT NEED AUTHENTICATION-####################################
*/
Auth::routes();

Route::get('/home', 'HomeController@index')
    ->name('home');
