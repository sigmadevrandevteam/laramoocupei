<?php

return 
[
	"default" =>
	[
		"name" 		=> "dev".mt_rand(5, 15).mt_rand(5, 15),
		"password" 	=> "user".mt_rand(5, 15),
		"email" 	=> "stg114@gmail.com",
	],
	"ToValidate" =>
	[
	    'compte' => [
	        'superadmin' => [   
	                            'email' => 'superadmin@gmail.com',
	                            'roles' => [
		                                    	['name'=> 'superadmin'],
		                                    	['name'=> 'admin'],
	                                    	],
	                            'name'  => 'superadmin',
	                            'password' => 'Ladmin@321',
	                            "is_valid" => true,
	                        ],
	        'admin' => [   
	                            'email' => 'admin@gmail.com',
	                            'roles' => [
	                                        	['name'=> 'admin'],
	                                    	],
	                            'name'  => 'admin',
	                            'password' => 'Ladmin@321',
	                            "is_valid" => true,
	                        ],
	        'teacher' => [   
	                            'email' => 'prf114@gmail.com',
	                            'roles' => [
	                                    		['name'=> 'teacher'],
	                                    	],
	                            'name'  => 'teacher',
	                            'password' => 'Ladmin@321',
	                            'is_valid' => true,
	                        ],
	        'etudiant' => [   
	                            'email' => 'participant114@gmail.com',
	                            'roles' => [
	                                    		['name'=> 'etudiant'],
	                                    	],
	                            'name'  => 'etudiant',
	                            'password' => 'Ladmin@321',
	                        ]

	    ],
	]
];

?>