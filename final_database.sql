-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2019 at 03:39 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(65, '2014_10_12_000000_create_users_table', 1),
(66, '2014_10_12_100000_create_password_resets_table', 1),
(67, '2019_10_14_114252_create_permission_tables', 1),
(69, '2019_10_15_150551_add_last_login_at_to_users_table', 1),
(70, '2019_10_15_150841_add_last_login_ip_to_users_table', 1),
(71, '2019_10_21_094646_put_new_column_to_users_table', 1),
(72, '2019_10_24_053154_create_forums_table', 1),
(73, '2019_10_24_081051_create_attachments_table', 1),
(74, '2019_10_24_083838_put_new_column_for_attachment', 1),
(75, '2019_10_24_131027_create_people_table', 1),
(76, '2019_10_24_132358_put_foreign_key_into_user_table', 1),
(77, '2019_10_28_075509_put_some_fields_into_people_table', 1),
(78, '2019_10_29_131444_put_some_column_into_the_table_people', 1),
(79, '2019_10_30_090205_create_other_infos_table', 1),
(80, '2019_10_30_140647_create_table_teacher', 1),
(81, '2019_10_31_113432_put_user_type_into_table_people', 1),
(83, '2019_10_31_132414_put_column_sex_into_table', 1),
(85, '2019_10_15_114209_create_courses_table', 1),
(94, '2019_11_05_153249_create_matters_table', 1),
(96, '2019_11_08_143728_put_column_into_users_table', 2),
(97, '2019_11_09_043318_create_image_table', 3),
(98, '2019_11_14_121702_create_studiants_table', 4),
(99, '2019_11_14_125348_add_table_assignation_cours', 5),
(100, '2019_11_29_081450_create_etudiants_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `person_id` bigint(20) UNSIGNED DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  `is_connected` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `person_id`, `avatar`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `last_login_at`, `last_login_ip`, `created_at`, `updated_at`, `is_valid`, `is_connected`) VALUES
(5, 4, NULL, 'dev512', '55@hotmail.com', NULL, '$2y$10$Fy4a805gVjKUXkUG8MOSH.0bbVJHyJNBSX6Y94cUrgtx5/DXWip82', NULL, NULL, NULL, '2019-11-04 10:35:04', '2019-11-04 10:35:04', NULL, 0),
(6, 3, NULL, 'dev1514', '1E@gmail.com', NULL, '$2y$10$SGhJfT23Y4pH.6rqJq/ByOIubPuBNslmazZgm0608aMAmParKpVy.', NULL, NULL, NULL, '2019-11-04 10:35:13', '2019-11-04 10:35:13', NULL, 0),
(7, 2, NULL, 'dev56', '960gd7@gmail.com', NULL, '$2y$10$UHTtpG6/ncSvxI2r11sRdePBdeb6MN7hjeuUmuUymDb02uAOvsstm', NULL, NULL, NULL, '2019-11-04 10:35:24', '2019-11-04 10:35:24', NULL, 0),
(10, NULL, NULL, 'superadmin', 'superadmin@gmail.com', NULL, '$2y$10$eghmtExe8Iu7HgUzx3ViwujGGJeOdiVvubJYeId2PY3ti6/dC.TAa', NULL, '2019-11-14 03:35:40', '127.0.0.1', '2019-11-08 14:53:20', '2019-11-14 03:55:22', 1, 0),
(11, NULL, NULL, 'admin', 'admin@gmail.com', NULL, '$2y$10$L6bFYBojPoeKVIHrYLRlr.ZmgFiTyLkOEHZoTltapLYXraF9FGCSK', NULL, '2019-12-13 04:55:30', '127.0.0.1', '2019-11-08 14:53:20', '2019-12-13 04:55:30', 1, 1),
(12, NULL, NULL, 'teacher', 'prf114@gmail.com', NULL, '$2y$10$ZUAMZFvwScssk6vEK/SW9OEMqgc8Vgttpr4V2QmsLL9wr0kVppD3C', NULL, '2019-11-18 04:15:08', '127.0.0.1', '2019-11-08 14:53:21', '2019-11-18 04:15:09', 1, 1),
(13, NULL, NULL, 'etudiant', 'participant114@gmail.com', NULL, '$2y$10$q2/UHZBRfwwm3EN5.Pvg3eSQQUhzy4g4y7fjbu4qb4JMqBJszzFr.', NULL, NULL, NULL, '2019-11-08 14:53:21', '2019-11-08 14:53:21', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_person_id_foreign` (`person_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
