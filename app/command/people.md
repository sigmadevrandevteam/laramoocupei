

# people cmd artisan

2019-10-29
------------------

php artisan migrate:rollback --path=\database\migrations\2019_10_28_075509_put_some_fields_into_people_table.php 


2019-11-08
-----------------
# list all user only who have  people binding ()

select p.name ,p.firstname, users.id from people as p left join users on users.person_id = p.id

# list all user even if they have not  people binding ()

select p.name ,p.firstname, users.id from users  left join people as p on users.person_id = p.id

# Add column teacher id into table courses:

alter table `courses` add `teacher_id` int null after `id`

# Put column into users table: 

alter table `users` add `is_valid` int null, add `is_connected` tinyint(1)  null



