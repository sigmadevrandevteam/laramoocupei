<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Forum extends Model
{
    protected $guarded = ["id"];
}
