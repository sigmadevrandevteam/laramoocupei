<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Studiant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    // table name
    protected $table = 'etudiants';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'name',
        'prename',
        'email',
        'adress',
        'postal_code',
        'created_at',
        'country',
    ];


}
