<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Attachment extends Model
{
	public $guarded = [];

    public function attachable()
    {
    	$this->morphTo();
    }
}
