<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Course extends Model
{

    protected $fillable = [

		"id",
		'user_id',
		"matter_id",
		'title',
		'content',
		'created_at',
		'assets1_id',
		'assets2_id',
		'assets3_id',
		'short_desc',
		'long_desc',
		'is_active',

    ];

    /**
	 * Get the post that owns the comment.
	 */
	public function matter()
	{
	    return $this->belongsTo('App\Models\Matter', 'matter_id');
	}

	public function teacher()
	{
		return $this->belongsTo('App\Models\Teacher', 'teacher_id');
	}

	public function image()
	{
		return $this->hasMany('App\Models\Image' , 'course_id','id');
	}

}

