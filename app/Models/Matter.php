<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Matter extends Model
{
    protected $fillable = [
    	'name',
    	'description',
    	'admin_id',
        'teacher_id',
    ];

    public function teacher()
    {
    	$this->belongsTo("App\Models\Teacher", "teacher_id");
    }

     public function courses()
    {
        return $this->hasMany('App\Models\Course','matter_id', 'id');
    }
}
