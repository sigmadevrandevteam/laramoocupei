<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class users extends Model
{

    // table name
    protected $table = 'people';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;
    protected $fillable =
        [
            "user_type",
            "sex",
            "name",
            "firstname",
            "email",
            "nationality",
            "birthday",
            "country",
            "city",
            "postalcode",
            "phone_number"
        ];
}
