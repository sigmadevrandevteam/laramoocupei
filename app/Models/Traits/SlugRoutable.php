<?php
namespace App\Models\Traits;


trait SlugRoutable {

	/**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    
}


?>