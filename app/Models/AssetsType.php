<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class AssetsType extends Model
{
    protected $fillable = ['name','description','is_active'];
    // public function getTableColumns($table) {
    //     return $this->getConnection()->getSchemaBuilder()->getColumnListing($table);
    // }

}
