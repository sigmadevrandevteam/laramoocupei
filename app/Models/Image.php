<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;


class Image extends Model
{


    protected $guarded = ['created_at'];
    protected $fillable = [
    	'size',
    	'name',
    	'cripted_name',
    	'course_id',
    	'teacher_id',
    	'type',

    ];


}
