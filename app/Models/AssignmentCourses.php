<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class AssignmentCourses extends Model
{
    protected $table = "assignment_courses";

    protected $fillable = [
    						'course_id',
    						'user_id'
    					];
}
