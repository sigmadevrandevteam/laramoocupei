<?php

namespace App\Models\ServicesModels;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class RoleServices
{
	protected $news;
	protected $olds;
	protected $user_id;
	protected $user;

	public function setUserId($user_id): self
	{
		$this->user_id = $user_id;
		return $this;
	}

	public function getUserId():integer
	{
		return $this->user_id;
	}

	public function setUser($user): self
	{
		$this->user = $user;
		return $this;
	}
	public function getUser(): User
	{
		return $this->user;
	}

	/**
	* Get all user role have in the database
	*
	* @return array
	*/
    public function findUserAllRole($id): array
    {
		$user = User::find(["id" => $id])->first();
		$items = [];

		if (! empty($user)) 
		{
			$user->getRoleNames()->each(function($item, $key) use(&$items) {
				array_push($items, $item);
			});
		}
		return $items;
    }

    /**
    * Put in user olds if there was data missing from the request
    * Put in user news if there was new data from the request
    *
    */
    public function checkMissUserRole($user_id, $request): self
    {
    	if ($request instanceof Request)
    	{
    		$datas = $request->all();
    	}

    	$from_request = isset($datas['role']) ? $datas['role'] : [];
    	$from_database = $this->findUserAllRole($user_id);

    	$user = User::find(['id' => $user_id])->first();
		$this->setUser($user);

    	$new_roles = [];
    	$old_roles = [];

		foreach ($from_request as $key => $role_id)
			{
				$role = Role::findById($role_id);

				if (!in_array($role->name, $from_database))
				{
					$new_roles [] = $role->name;
				}
				
			}


		foreach ($from_database as $key => $role_name) {

			$role = Role::findByName($role_name);

			if (!in_array($role->id, $from_request))
				{
					$old_roles [] = $role->name;
				}
		}

		$this->user_id = $user_id;
		$this->news = $new_roles;
		$this->olds = $old_roles;

		return $this;
    }

    /**
    *
    *
    */
    public function putNewUserRolesRemoveOlds($user_id = null)
    {
    	$boolean = true;

    	$user = $this->getUser();
 
		if (!empty($this->news))
		{
			foreach ($this->news as $key => $role_name) {
		
				if(! $user->hasRole($role_name))
				{
					$user->assignRole($role_name);
				}
			}
		}

		if (!empty($this->olds))
		{
			foreach ($this->olds as $key => $role_name) {

				if($user->hasRole($role_name))
				{
					$user->removeRole($role_name);
				}
			}
		}

		return $boolean;
	
    }

	public function setNew(array $roles = [])
	{
		if (! empty($roles) )
		{
			$this->news = $roles;
		}

		$user = $this->getUser();
 
		if (!empty($this->news))
		{
			foreach ($this->news as $key => $role_name) {
		
				if(! $user->hasRole($role_name))
				{
					$user->assignRole($role_name);
				}
			}
		}
	}

	public function setOlds(array $roles)
	{
		$this->olds = $roles;
	}
	
	public function searchEach(array $roles, array $searches, $role_key = 'name')
	{
		$not_yet_present = [];
		
		// each from  users databse
		foreach ($roles as $key => $role) { 

			if ( isset($role[$role_key]) )
			{
				if(! in_array( $role[$role_key] , $searches)) 
				{
					$not_yet_present [] = $role;
				}
			}

		}

		return $not_yet_present;
	}

	/**
	 * Create Roles
	 */
	public function create($roles = [], $key_role= 'name')
	{
		$roles_in_database = Role::all();
		$roles_from_database = [];
		foreach ($roles_in_database as  $all) {
			$roles_from_database[$key_role][] = $all->$key_role;
		}
		if (empty($roles_from_database))
		{
			$roles_from_database[$key_role] = [];
		}


		if (!empty($roles))
		{

			if (!isset($roles[$key_role]))
			{
				foreach ($roles as $key => $role) {

					if(!in_array( $role[$key_role],  $roles_from_database[$key_role]))
					{
						Role::create([$key_role => $role[$key_role] ]);
					}
				}
			}else if(isset($roles[$key_role])){
				
				if(!in_array( $roles[$key_role], $roles_from_database[$key_role]))
					{
						Role::create([$key_role => $roles[$key_role] ]);
					}
				}
		}
	}

}
