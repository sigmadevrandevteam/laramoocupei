<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssocAssetsCourse extends Model
{
    protected $table ='assoc_assets_course';

    protected $guarded = ['id'];
}
