<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Teacher extends Model
{
    protected $fillable = [
    		"person_id",
    		'matter',
    		'class',
    	];

}
