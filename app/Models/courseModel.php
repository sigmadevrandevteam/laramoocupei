<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class courseModel extends Model
{
    // table name
    protected $table = 'courses';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;
}
