<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Superadmin extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'prename',
        'email',
        'adress',
        'postal_code',
        'created_at',
        'country',
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
