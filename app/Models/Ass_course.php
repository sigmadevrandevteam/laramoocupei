<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Ass_course extends Model
{
    // table name
    protected $table = 'student_course_list';
    
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'student_id',
        'student_course',
    ];
}
