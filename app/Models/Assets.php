<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use Carbon;
use Collective\Html\Eloquent\FormAccessible;


class Assets extends Model
{
    protected $fillable =
    [
    	'id_type',
    	'title',
    	'short_description',
    	'long_description',
    	'assets_filename',
    	'assets_fileurl',
    	'image_1'
    ];

    // public function getTableColumns($table) {
    //     return $this->getConnection()->getSchemaBuilder()->getColumnListing($table);
    // }
}
