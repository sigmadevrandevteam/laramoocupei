<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Role extends Model
{

    protected $fillable = ['name', 'gurad_name', 'created_at'];

}
