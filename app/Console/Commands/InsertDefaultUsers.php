<?php
namespace App\Console\Commands;

use App\User;
use  App\Models\ServicesModels\RoleServices;
use Illuminate\Support\Facades\DB;

use Illuminate\Console\Command;

class InsertDefaultUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mooc:insertDefaultUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert user default in database  ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        $users_in_config = config('users.ToValidate.compte');
        $new_user_added = [];

        foreach($users_in_config as $config_user):
           
            $RoleServices = new RoleServices();
            if (isset($config_user['email']))
            {
                $user = User::where(['email'=> $config_user['email']])->first();

                if ($user == null && $config_user['email'] != null)
                {
                    
                    User::create([
                        'name' => $config_user['name'],
                        'email' => $config_user['email'],
                        'password' => bcrypt($config_user['password']),

                    ]);
                    
                    $new_user_added[] = [
                        'email' => $config_user['email'],
                        'name'  => $config_user['name'],
                        'roles' => $config_user['roles'],
                        'password' => $config_user['password'],
                    ];
                }
                

                $user = User::where(['email'=> $config_user['email']])->first();
                
                if ($user)
                {   
                    $RoleServices->setUser($user);
                    $UserRoles = $RoleServices->findUserAllRole($user->id);
                    // each config users file
                    $role_not_yet_in_database = $RoleServices->searchEach($config_user['roles'], $UserRoles);

                    $RoleServices->create($config_user['roles']);
                    $RoleServices->setNew($role_not_yet_in_database);
                }
            }

        endforeach;
        
        if (count($new_user_added) > 0)
        {
            foreach ($new_user_added as $key => $user) {
                $this->info($user['name']. " was created");
            }
        }
        $this->info('Insertion was been successuly finished');
    }
}
