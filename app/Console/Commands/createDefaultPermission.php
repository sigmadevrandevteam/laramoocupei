<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;

class createDefaultPermission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mooc:createDefaultPermission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CreateDefaultPermission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = 0;
        

        foreach (config('permission.permission') as $key => $permissions) {
            
            if (is_array($permissions))
            {

                
                foreach ($permissions as $key => $permission) {
                    app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
           
                    if (Permission::getPermissions(['name' => $permission['name']])->first() == null)
                    {
                        Permission::findOrCreate($permission['name']);
                        $count++;
                    }else{
                        // Permission::getPermissions(['name' => $permission['name']])->first()->delete();
                    }
                }
            }
           
        }
        $this->info($count.' permission was been successuly created');
    }
}
