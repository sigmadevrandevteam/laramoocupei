<?php

namespace App\Http\Controllers\BackOffice;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Models\AssetsType;
use App\Models\Assets;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use DateTime;
class AssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(5);
        $roles = Role::all();
        return view("backOffice.users.index",[
            "users" => $users,
            "roles" => $roles
        ]);
    }

   public function type_list(){

    $users = User::paginate(5);
    $roles = Role::all();

    $assets_typeList = AssetsType::all();
    return view('backOffice.assets.list_type',[
        "users" => $users,
        "roles" => $roles,
        "assets_type" => $assets_typeList
    ]);

   }
    public function addtype(Request $request){
        $request->validate([
            'name'=>'required'
        ]);

        $assetstype = new AssetsType([
            'name' => $request->get('name'),
            'is_active' => $request->get('is_active'),
            'description' => $request->get('description'),
        ]);

        $saved = $assetstype->save();
        if($saved){
            return redirect()->route('superadmin.assets.typeList');
        }
    }
    public function deletetype($id){
        $deletetype = AssetsType::find($id);
        $deleted = $deletetype->delete();
        if($deleted){
            return redirect()->route('superadmin.assets.typeList');
        }
    }
    public function list(){

    $users = User::paginate(5);
    $roles = Role::all();
    $assets_list = Assets::all();
        return view('backOffice.assets.list',[
            "users" => $users,
            "roles" => $roles,
            "assets" => $assets_list
        ]);
    }
    public function save(Request $request){
    
        $type_id = $request->get('type_id');
        $assets_user_id = $request->get('assets_user_id');

        $assetsTypeData = AssetsType::find(['id' => $type_id])->first();
        
        $path = public_path('course_assets/').$assets_user_id;
        $path_url = "course_assets/".$assets_user_id;

        if (!is_dir($path.'/'.$assetsTypeData->name)) {
            File::makeDirectory($path.'/'.$assetsTypeData->name,0777,true);    
        }
        if (!is_dir($path.'/image')) {
            mkdir($path.'/image');
            File::makeDirectory($path,0777,true);      
        }
        $file = $request->file('assets_fileurl');
        $new_file_name = rand().'.'.$file->getClientOriginalExtension();
        $file->move($path.'/'.$assetsTypeData->name, $new_file_name);
        
        $image = $request->file('image1');
        $new_image_name = rand().'.'.$image->getClientOriginalExtension();
        $image->move($path.'/image', $new_image_name);

        
        $title = $request->get('title');
        $short_description = $request->get('short_description');
        $long_description = $request->get('long_description');
        $assets_fileurl = $path_url.'/'.$assetsTypeData->name.'/'.$new_file_name;
        
        $image_1 = $path_url.'/'.$assetsTypeData->name.'/'.$new_image_name;
        $dt = new DateTime();
        $fields = array(
            "type_id" => $type_id ,
            "title" => $title ,
            "short_description" => $short_description ,
            "long_description" => $long_description ,
            "assets_filename" => $new_file_name ,
            "assets_fileurl" => $assets_fileurl ,
            "image_1" => $image_1 ,
            "assets_user_id" => $assets_user_id ,
            "created_at" => $dt->format('Y-m-d H:i:s')
        );

        $data = DB::table('assets')->insertGetId($fields);
        $fields_assoc_assets_user = array(
            "id_assets" => $data,
            "created_at" => $dt->format('Y-m-d H:i:s'),
        );
        $data_assoc = DB::table("assoc_assets_course")->insertGetId($fields_assoc_assets_user);
        $data_returned = ["asset_id" => $data_assoc,"titre" => $title];
        return $data_returned;
    } 
}
