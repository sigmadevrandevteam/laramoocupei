<?php

namespace App\Http\Controllers\BackOffice;

use App\User;
use App\Models\Person;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;

use DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(5);

        $roles = Role::all();

        
        // dd(config('permission.users.superadmin'));
        $person_form = config('form.person');

        // DB::select('
        //                     select * from people as p
        //                     left join users as u
        //                     on u.person_id = p.id
        //                     where u.person_id is  null
        //                     ORDER BY u.person_id DESC
        //                     ');

        $people = Person::select("people.id","people.name","people.email","people.user_type")
                    ->leftjoin("users", "users.person_id","people.id")
                    ->where('users.person_id','=',NULL)
                    ->orderBy("people.id", "desc")
                    ->get();

    

        return view("backOffice.users.index",[
            "users" => $users,
            "roles" => $roles,
            "person_form" => $person_form['fr'] ,
            "people" => $people
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request = $request->all();
        $person = $request['person'];

        $person_in_database = Person::where(["id" => intval($person['people_email'])])->first();



         $user = User::create([
            "name"      => $person['name'],
            "password"  => bcrypt($person['password']),
            "person_id" => $person_in_database->id,
            "email"     => $person_in_database->email,
        ]);

        if ($person_in_database->user_type == \App\Models\Person::TEACHER)
        {
            if(! $user->hasRole('teacher'))
            {
                $user->assignRole('teacher');
            }
        }

        if ($person_in_database->user_type == \App\Models\Person::STUDIANT)
        {
            if(! $user->hasRole('etudiant'))
            {
                $user->assignRole('etudiant');
            }
        }
        

        Session::put("person.name", $person['name']);
        Session::put("person.name", $person['password']);
        Session::put("person.people_email", $person['people_email']);
        Session::save();

        return redirect()->route("admin.users.index");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
