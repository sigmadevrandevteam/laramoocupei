<?php

namespace App\Http\Controllers\BackOffice;

use App\Models\Course;
use App\Models\Studiant;
use App\Models\users;
use App\User;
use App\Models\Person;
use App\Models\DegreeLevel;
use Spatie\Permission\Models\Role;
use App\Models\ServicesModels\RoleServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;

class StudiantController extends Controller
{

  public function __construct()
  {
    $this->roleServices = new \App\Models\ServicesModels\RoleServices();
  }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Person::STUDIANT;

        $studiants = Person::select("people.*")
                      ->where('user_type',"=", Person::STUDIANT)
                      ->orderBy('id','desc')
                      ->paginate(5);

                    // ->orderBy('users.id','desc');
        $person_form = config('form.person');


        return view("backOffice.studiant.index",[
            "studiants"   => $studiants,
            "person_form" => $person_form["fr"],
        ]);
    }


    public  function coures_list(){

        $courses = Course::paginate(5);

        return view('backOffice.studiant.course_list', [
            "courses" => $courses
        ]);
    }
    public function store(Request $request)
    {
        $stud = Person::STUDIANT ;


        $this->validate($request, [
            'Adresse' => 'required',
            'Ville' => 'required',
            'Nom' => 'required',
            'Prenom' => 'required',
            'Email' => 'required',
            'nationality' => 'required',
            'Code_Postal' => 'required',
            'phone_number' => 'required',
        ]);
        // create student
        $users = new Studiant();
        $users->adress = $request->input('Adresse');
        $users->country = $request->input('Ville');
        $users->fname = $request->input('Nom');
        $users->lname = $request->input('Prenom');
        $users->email = $request->input('Email');
        $users->postal_code = $request->input('Code_Postal');

        $users->save();

        // save to people table
        $people = new users();
        $people->adress = $request->input('Adresse');
        $people->city = $request->input('Ville');
        $people->name = $request->input('Nom');
        $people->firstname = $request->input('Prenom');
        $people->email = $request->input('Email');
        $people->postalcode = $request->input('Code_Postal');
        $people->birthday = $request->input('birthday');
        $people->user_type = $stud;
        $people->sex = $request->input('sex');

        $people->save();
        return redirect()->route("manager.studiant.user")
                        ->with('success', 'Students Created');

    }


    public function delete ($id)
    {

      return redirect()->route('manager.studiant.user');
    }



    /**
     * Edition of the person
     */
    public function edit ($id)
    {

      $studiant = Person::select("people.*","degree_levels.*","people.id as uid")
                  ->leftjoin("degree_levels", "degree_levels.studiant_id", "people.id")
                  ->first();

      // dd($studiant->toArray() );

      $person_form = config('form.person');

      return view("backOffice.studiant.edit",[
          "studiant"    => $studiant,
          "person_form" => $person_form["fr"],
      ]);
    }

    /**
     * Store data after editing the posted
     */
    public function save_edit ( Request $request, $id)
    {




      $person_from_request       = $request->get('person');
      $degree_level_from_request = $request->get('degree_level');

      $rules = [
          'firstname' => ['required'],
          'email'=>['required','regex:/(.+)@[a-zA-Z]{3,}\.[a-zA-Z]{2,3}/i']
        ];

      $validator = Validator::make($person_from_request, $rules)->validate();

      $studiant   = Person::where(["id" => intval( $person_from_request['id'])])->first();
      $level      = DegreeLevel::where(["studiant_id" => intval( $person_from_request['id'])])->first();

      $studiant = $studiant->toArray();
      $level    = $level->toArray();

      if (isset($studiant['created_at']))
        unset($studiant['created_at']);

      if (isset($studiant['updated_at']))
        unset($studiant['updated_at']);

      if (isset($level['created_at']))
        unset($level['created_at']);

      if (isset($level['updated_at']))
        unset($level['updated_at']);

      $diff       = array_diff($person_from_request, $studiant);
      $diff_level = array_diff( $degree_level_from_request, $level);

      dump($diff);
      dump($diff_level);


      exit;

      if (!empty ($diff))
      {
        if (isset($diff['postalcode']))
        {
           $diff['postalcode'] = substr($diff['postalcode'], 0, 5);
        }
         Person::where('id', $id)->update($diff);
      }

      return redirect()->route("manager.studiant.user");


    }
}
