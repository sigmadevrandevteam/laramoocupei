<?php

namespace App\Http\Controllers\BackOffice\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class TeacherAdminController extends Controller
{

	public function ajax_teacher($id)
	{
		$datas = [];

		  $columns = [
                        'u.*',
                        'u.id as uid',
                        'u.name',
                        'u.email',
                        'r.name as role_name',
                        'ac.user_id'
                    ];


		$teacher = \App\User::from('users as u')
            ->select($columns)
            ->leftjoin('model_has_roles as m_r','m_r.model_id','u.id')
            ->leftjoin('roles as r', 'r.id','m_r.role_id')
            ->leftjoin('assignment_courses as ac', 'ac.user_id','u.id')

            ->where([
                'r.name' => 'teacher'
                ])
            ->where('ac.user_id','=',null)

            ->where('u.id', '=', $id)
            ->first()
    
           ;

		$datas  = json_encode($teacher);

		return new Response($datas);
	}
}