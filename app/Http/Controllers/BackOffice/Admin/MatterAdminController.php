<?php

namespace App\Http\Controllers\BackOffice\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class MatterAdminController extends Controller
{
    protected function Voter($matter)
    {
        $merge = [];

        $id = auth()->user()->id;

        if(auth()->user()->hasRole('admin'))
        {
            $merge = [
                'admin_id' => $id 
            ];
        }

        if (auth()->user()->hasRole('teacher'))
        {
            $merge = [
                'teacher_id' => $id
            ];
        }

        return array_merge($matter, $merge);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacher_id = auth()->user()->id;

        $matters = \App\Models\Matter::select('matters.*')

                ->paginate(5);

                // dd($matters);
                // $matters = [];
                // exit;
        
        return view('backOffice.matter.admin.index', [
            'matters' => $matters
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backOffice.matter.admin.new');
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $matter = $request->get('matter');

        $matter = $this->Voter($matter);

        $rules = [
            'name'          => ['required'],
            'description'   => ['required'],
         ];


        Validator::make($matter, $rules)->validate();

        \App\Models\Matter::create($matter);

        return redirect()->route('bo.prof.matter.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matter = \App\Models\Matter::where([
                'id'    => $id
            ])->first();
        return view('backOffice.matter.admin.edit',[
            'matter'    => $matter,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $matter  = $request->get('matter');

        $matter = $this->Voter($matter);

        $matter_from_database = \App\Models\Matter::where([
            'id' => $id
            ])->first()->toArray();

        $rules = [
            'name'          => ['required'],
            'description'   => ['required'],
         ];


        Validator::make($matter, $rules)->validate();

        $diff = array_diff($matter, $matter_from_database);

        \App\Models\Matter::where('id', $id)->update($diff);

        return redirect()->route('bo.prof.matter.list');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $matter = \App\Models\Matter::select('matters.*','courses.id as course_id')
        //         ->leftjoin('courses', 'matters.id','courses.matter_id')
        //         ->where('matters.id', $id)->get();

        $matter = \App\Models\Matter::where(['id' =>$id])->first();

        
        $courses = $matter->courses()->get();

        return view('backOffice.matter.admin.destroy',
            [
                'matter'    => $matter,
                'courses'   => $courses,
            ]);
    }
    /**
    * This method allow to delete all cours who are binding with course
    * 
    */
    public function delete(Request $request, $id)
    {
        $courses = $request->get('courses');

        if (!empty($courses)) {
            foreach($courses as $course)
            {
                \App\Models\Course::where('id', $course)->delete();
            }
        }

        \App\Models\Matter::where('id', $id)->delete();

        return redirect()->route('bo.prof.matter.list');
    }


    public function course_destroy (Request $request, $id, $matter_id)
    {
        \App\Models\Course::where('id', $id)->delete();

        return redirect()->route('bo.prof.matter.admin.destroy', $matter_id);
    }

    public function actions ($id)
    {

        // $teachers = \App\User::from('users as u')
        //     ->select('u.*')
        //     ->leftjoin('people as p', 'u.person_id','p.id')
        //     ->where([
        //         'p.user_type' => \App\Models\Person::TEACHER
        //     ])
        //     ->get();

        $teachers = \App\User::from('users as u')
            ->select('u.id','mr.model_id','u.name','p.firstname')
            ->leftjoin('people as p', 'p.id','u.person_id')
            ->leftjoin('model_has_roles as mr','mr.model_id','u.id')
            ->leftjoin('roles as r','r.id', 'mr.role_id')
            ->where('r.name','=','teacher')
            ->get();


        $assigned = \App\Models\Matter::where(['teacher_id' => $id])->first();

        return view('backOffice.matter.admin.actions',[
                'teachers'  => $teachers,
                'id'        => $id, 
                'assigned'  => $assigned,
            ]);
    }

    public function assigner (Request $request, $id)
    {

        $matter_assignment = $request->get('matter_assignment');

        // dd($request->get('matter_assignment'));

        if(intval($matter_assignment['action']) === 2)
        {
            $assignment = 0;
        }elseif(intval($matter_assignment['action']) === 1)
            {
                $assignment = 0;
            }

        /**
        * Assignation sur le teacher_id depuis son compte
        */
        \App\Models\Matter::where(['id' =>$id])->update([
            'teacher_id' => intval($matter_assignment['teacher']),
            ]);

        return redirect()->route('bo.prof.matter.show', $id);
    }

    
}
