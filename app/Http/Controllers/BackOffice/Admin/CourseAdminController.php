<?php

namespace App\Http\Controllers\BackOffice\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = \App\Models\Course::paginate(5);

        $columns = [
                        'c.id',
                        't.id as tid',
                        'c.title',
                        't.name'
                    ];

        $courses = \App\Models\Course::from('courses as c')
            ->select($columns)
            ->leftjoin('assignment_courses as a_c', 'a_c.course_id','c.id')
            ->leftjoin('users as t', 't.id', 'a_c.user_id')
            ->leftjoin('assignment_courses as ac', 'ac.user_id','t.id')
            // ->where([
            //     'r.name' => 'teacher'
            //     ])

            ->paginate(5)
           ;


       $columns = [
                    'u.person_id',
                    'u.id as uid',
                    'u.name',
                    'u.email',
                    'r.name as role_name',
                    'ac.user_id'
                ];

        $teachers = \App\User::from('users as u')
            ->select($columns)
            ->leftjoin('model_has_roles as m_r','m_r.model_id','u.id')
            ->leftjoin('roles as r', 'r.id','m_r.role_id')
            ->leftjoin('assignment_courses as ac', 'ac.user_id','u.id')

            ->where([
                'r.name' => 'teacher'
                ])
            ->where('ac.user_id','=',null)

            ->get()
    
           ;


        $person_form = config('form.person');

        return view('backOffice.course.admin.index', [
            "courses"       => $courses,
            "person_form"   => $person_form['fr'] ,
            "teachers"      => $teachers,
        ]);
    }

    public function manager()
    {
        return view('backOffice.course.admin.manager',[
            
        ]);
    }

    public function actions(Request $request, $id)
    {
        $columns = [
                        'u.person_id',
                        'u.id as uid',
                        'u.name',
                        'u.email',
                        'r.name as role_name'
                    ];

        $teachers = \App\User::from('users as u')
            ->select($columns)
            ->leftjoin('model_has_roles as m_r','m_r.model_id','u.id')
            ->leftjoin('roles as r', 'r.id','m_r.role_id')
            ->leftjoin('assignment_courses as ac', 'ac.user_id','u.id')

            // ->where([
            //     'r.name' => 'teacher'
            //     ])
            // ->where('u.id','=','ac.user_id')

            ->get()
           ;


        $course = \App\Models\Course::where(['id' => $id])->first();

        return view('backOffice.course.admin.actions',[
            'id'        => $id,
            'course'    =>  $course,
            'teachers'  => $teachers
        ]);
    }

    public function save_assignment(Request $request)
    {

        $assignment_course = $request->get('assignment_course');
        $course_id = $assignment_course['course_id'];

        // dd($assignment_course );
        
        if (isset($assignment_course['user_id']) && intval($assignment_course['user_id']) == 0)
        {
            return redirect()->route('bo.admin.course.actions', ['id'=> $course_id]);
        }

        \App\Models\AssignmentCourses::create($assignment_course);


        return redirect()->route('bo.admin.course.index');
    }

    public function rupture_assignation(Request $request)
    {
        $rupture = $request->get('rupture_course');

        \App\Models\AssignmentCourses::where([
            'course_id' => $rupture['course_id']
        ])
            ->delete();

        return redirect()->route('bo.admin.course.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
