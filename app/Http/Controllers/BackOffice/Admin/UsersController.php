<?php

namespace App\Http\Controllers\BackOffice\Admin;

use App\User;
use App\Models\Person;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;

use DB;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::from('users as u')
        			->select('u.is_connected','u.is_valid','u.id', 'u.name','p.id as pid', 'p.firstname')
        			->leftjoin('people as p', "u.person_id","p.id")
        			->paginate(5);

        $roles = Role::all();

        $person_form = config('form.person');

        $people = Person::select("people.id","people.name","people.email","people.user_type")
                    ->leftjoin("users", "users.person_id","people.id")
                    ->where('users.person_id','=',NULL)
                    ->orderBy("people.id", "desc")
                    ->get();


        return view("backOffice.users.admin.index",[
            "users" => $users,
            "roles" => $roles,
            "person_form" => $person_form['fr'] ,
            "people" => $people
        ]);
    }
}