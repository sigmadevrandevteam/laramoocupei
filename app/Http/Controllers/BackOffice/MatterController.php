<?php

namespace App\Http\Controllers\BackOffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class MatterController extends Controller
{   

   
    protected function Voter($matter)
    {
        $merge = [];

        $id = auth()->user()->id;

        if(auth()->user()->hasRole('admin'))
        {
            $merge = [
                'admin_id' => $id 
            ];
        }

        if (auth()->user()->hasRole('teacher'))
        {
            $merge = [
                'teacher_id' => $id
            ];
        }

        return array_merge($matter, $merge);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacher_id = auth()->user()->id;

        $matters = \App\Models\Matter::select('matters.*')

                ->paginate(5);

                // dd($matters);
                // $matters = [];
                // exit;
        
        return view('backOffice.matter.index', [
            'matters' => $matters
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backOffice.matter.new');
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $matter = $request->get('matter');

        $matter = $this->Voter($matter);

        $rules = [
            'name'          => ['required'],
            'description'   => ['required'],
         ];


        Validator::make($matter, $rules)->validate();

        \App\Models\Matter::create($matter);

        return redirect()->route('bo.prof.matter.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matter = \App\Models\Matter::where([
                'id'    => $id
            ])->first();
        return view('backOffice.matter.edit',[
            'matter'    => $matter,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $matter  = $request->get('matter');

        $matter = $this->Voter($matter);

        $matter_from_database = \App\Models\Matter::where([
            'id' => $id
            ])->first()->toArray();

        $rules = [
            'name'          => ['required'],
            'description'   => ['required'],
         ];


        Validator::make($matter, $rules)->validate();

        $diff = array_diff($matter, $matter_from_database);

        \App\Models\Matter::where('id', $id)->update($diff);

        return redirect()->route('bo.prof.matter.list');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $matter = \App\Models\Matter::select('matters.*','courses.id as course_id')
        //         ->leftjoin('courses', 'matters.id','courses.matter_id')
        //         ->where('matters.id', $id)->get();

        $matter = \App\Models\Matter::where(['id' =>$id])->first();

        
        $courses = $matter->courses()->get();

        return view('backOffice.matter.destroy',
            [
                'matter'    => $matter,
                'courses'   => $courses,
            ]);
    }
    /**
    * This method allow to delete all cours who are binding with course
    * 
    */
    public function delete(Request $request, $id)
    {
        $courses = $request->get('courses');

        foreach($courses as $course)
        {
            \App\Models\Course::where('id', $course)->delete();
        }

        \App\Models\Matter::where('id', $id)->delete();

        return redirect()->route('bo.prof.matter.list');
    }


    public function course_destroy (Request $request, $id, $matter_id)
    {
        return redirect()->route('bo.prof.matter.destroy', $matter_id);
    }
}
