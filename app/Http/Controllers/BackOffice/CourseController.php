<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Course;
use App\Models\Teacher;
use App\User;
use App\Models\AssetsType;
use App\Models\Assets;
use DateTime;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{

	protected function getMatter()
	{
		$auth_id = auth()->user()->id;

		if (auth()->user()->hasRole('teacher'))
		{

			$matters = \App\Models\Matter::where([
				'teacher_id' => $auth_id,
			])->get();
		}

		if (auth()->user()->hasRole('admin'))
		{
			$matters = \App\Models\Matter::where([
				'admin_id' => $auth_id,
			])->get();

		}

		return $matters;
	}

	public function manager()
	{
		return view('backOffice.course.manager');
	}


	public function index()
	{

		// exit;
		// $permission = Permission::findByName('edit articles');
		// $role = Role::findByName( 'edit');

		// $role = Role::findById(1);
		// $permission = Permission::findById(2);
		// $role->givePermissionTo($permission);
		// $permission->removeRole($role);

		// auth()->user()->givePermissionTo('show articles');
		// auth()->user()->assignRole('show');

		// return auth()->user()->permissions;

		$courses = \App\Models\Course::paginate(5);

		return view('backOffice.course.index', [
			"courses" => $courses
		]);
	}

	public function list()
	{

		// exit;
		$users = User::paginate(5);
		$roles = Role::paginate(5);
		$course_list = Course::all();
		return view("backOffice.course.list", [
			"course_lists" => $course_list,
			"roles" => $roles,
			"users" => $users,
		]);
	}


	public function edit(Request $request, $id)
	{
		$course = \App\Models\Course::where([
			'id' => $id
			])->first();


		$matters = $this->getMatter();
		

		return view('backOffice.course.edit',[
			'course' 	=> $course,
			'matters' 	=> $matters,
		]);
	}

	/**
	* Create Course with matter binding with the course
	* 
	*/
	public function create ()
	{
		$teacher_id = auth()->user()->id;

		$matters = $this->getMatter();

		$is_empty = false;

		array_walk($matters, function($items, $key) use(&$is_empty) {
			$is_empty = empty($items) ? true : false;

		});

		return view('backOffice.course.new',[
			'matters'  => $matters,
			'is_empty' => $is_empty,
		]);
	}

	public function store(Request $request)
	{
		$course = $request->get('course');

		$teacher_id = auth()->user()->id;
		$course = array_merge($course, [
			'teacher_id' => $teacher_id
		]);
		

		$rules = [
			'matter_id' => ['required'],
            'title' 	=> ['required'],
            'content'	=> ['required'],
         ];

      	$messages = ["content"=>"Le contenu ne doit pas être vide"];

      	if (intval($course['matter_id']) == 0)
      	{
      		return redirect()->route('bo.prof.course.list');
      	}

      	Validator::make($course, $rules, $messages)->validate();


		\App\Models\Course::create($course);

		return redirect()->route('bo.prof.course.list');
	}

	public function update(Request $request, $id)
	{
		$course = $request->get('course');

		$teacher_id = auth()->user()->id;

		$course = array_merge($course, [
			'teacher_id' => $teacher_id
		]);
		
		$course_from_database = \App\Models\Course::where(['id' => $id])
			->first();

		$diff = array_diff($course, $course_from_database->toArray());

		\App\Models\Course::where(['id' => $id])->update($diff);

		return redirect()->route('bo.prof.course.list');
	}

	/**
	*
	*
	*/
	public function destroy (Request $request, $id)
	{

		\App\Models\Course::where('id', $id)->delete();
		return redirect()->route('bo.prof.course.list');
	}


	public function add()
	{
		$users = User::paginate(5);
		$roles = Role::paginate(4);
		$course_list = Course::all();
		$assets_type = AssetsType::all();
		$teachers = Teacher::all();
		return view("backOffice.course.addform", [
			"course_lists" => $course_list,
			"roles" => $roles,
			"users" => $users,
			"assets_type" => $assets_type,
			"course" => null,
			"teachers" => $teachers,
			"assets_list" => null,
		]);
	}

	public function savecourse(Request $request)
	{
		request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

		$fields = $request->get('course');

		if (!isset($fields['matter_id']))
		{
			$fields['matter_id'] = 1;
		}

		$user_id = $fields['user_id'];
		$path = public_path('course_assets/image/').$user_id;
		
		if ( !is_dir($path) ) {
			File::makeDirectory($path,0777,true);
        }

		$path_url = "course_assets/image/".$user_id;
		$image_file = request()->image;
		if($image_file){
		$new_file_name = rand().'.'.$image_file->getClientOriginalExtension();
		$image_file->move($path, $new_file_name);

		$image = $path_url.'/'.$new_file_name;

		$fields['image'] = $image;
		}
		$dt = new DateTime();
		$fields['created_at'] = $dt->format('Y-m-d H:i:s');
		$id_assets = $fields['assets_list_id'];
		$idcourse = DB::table('courses')->insertGetId($fields);
		DB::table('assoc_assets_course')
			->where('id', $id_assets)
			->update(['id_course' => $idcourse]);
		return redirect()->route('admin.course.list');
	}

	public function delete($id)
	{
		$coursedata = DB::table('courses')
			->where('id', $id)
			->get()
			->first();
		$id_course_assoc = $coursedata->id;
		DB::table('courses')
			->where('id', $id)
			->delete();
		DB::table('assoc_assets_course')
			->where('id_course', $id_course_assoc)
			->delete();
		return redirect()->route('admin.course.list');
	}

	public function updateForm($id)
	{
		$users = User::paginate(5);
		$roles = Role::paginate(4);
		$course_list = Course::all();
		$assets_type = AssetsType::all();
		$teachers = Teacher::all();
		$assets_assoc = DB::table('assoc_assets_course')
			->join('assets', 'assoc_assets_course.id_assets', '=', 'assets.id')
			->select('assets.*')
			->where('id_course', $id)
			->get();
		//dd($assets_assoc);

		$coursedata = DB::table('courses')
			->where('id', $id)
			->get()
			->first();

		return view("backOffice.course.addform", [
			"course_lists" => $course_list,
			"roles" => $roles,
			"users" => $users,
			"assets_type" => $assets_type,
			"course" => $coursedata,
			"teachers" => $teachers,
			"assets_list" => $assets_assoc,
		]);
	}
	
	public function updatecourse(Request $request)
	{

		$fields = $request->get('course');
		$id = $fields['id'];
		$user_id = $fields['user_id'];

		$path = public_path('course_assets/image/').$user_id;

		if ( !is_dir($path) ) {
			File::makeDirectory($path,0777,true);
        }

		$path_url = "course_assets/image/".$user_id;
		$image_file = request()->image;
		if($image_file){
			$new_file_name = rand().'.'.$image_file->getClientOriginalExtension();
			$image_file->move($path, $new_file_name);
			$image = $path_url.'/'.$new_file_name;
			$fields['image'] = $image;
		}
		$dt = new DateTime();
		
		$fields['updated_at'] = $dt->format('Y-m-d H:i:s');
		DB::table('courses')
			->where('id', $id)
			->update($fields);
		
		DB::table('assoc_assets_course')
			->where('id', $fields['assets_list_id'])
			->update(['id_course' => $fields['id']]);
		
		return redirect()->route('admin.course.list');
	}

	public function delete_image(Request $request){
		dd($request->get('id_course'));
	}

}
