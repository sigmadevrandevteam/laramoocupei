<?php

namespace App\Http\Controllers\BackOffice;


use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;


class RoulesController extends Controller
{


	public function index()
	{
		// $role = Role::create(['name' => 'writer']);
		// $permission = Permission::create(['name' => 'write articles']);

		// $role = Role::findById(1);

		// $role = Role::create(['name' => 'prof']);
		// $permission = Permission::findById(2);
		// $role->givePermissionTo($permission);
		// $permission->removeRole($role);

		// auth()->user()->givePermissionTo('write articles');
		// auth()->user()->assignRole('writer');

		// return auth()->user()->permissions;



		return view("backOffice.roles.list");
	}
	
	public function manage()
	{

		$roles = Role::paginate(4);
		
		return view("backOffice.roles.list",[
			"roles" => $roles
		]);
	}
}
?>