<?php

namespace App\Http\Controllers\BackOffice;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index($id = null)
    {
    	if ($id != null || $id !== 0)
    	{
    		$user = User::find(['id' => $id])->first();
    	} else
    		{
    			return redirect()->route('error404');
    		}
        if ($user->avatar == null)
        {
            $user->avatar = "adminLTE/dist/img/user4-128x128.jpg";
        }


    	return view('backOffice.users.profile',[
    		'user' => $user
    	]);
    }


}
