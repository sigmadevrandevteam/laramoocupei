<?php

namespace App\Http\Controllers\BackOffice;
use App\Models\Forum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForumController extends Controller
{
    
    public function index()
    {
        $forums = Forum::paginate(5);
        return view('backOffice.forum.index',
        compact('forums')
        );
    }
}
