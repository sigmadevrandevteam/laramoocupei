<?php

namespace App\Http\Controllers\BackOffice;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Models\Subject;
use App\User;
use DateTime;
use Illuminate\Support\Facades\DB;
use App\Models\Course;
use App\Models\Teacher;
class SubjectController  extends Controller
{
    public function List()
    {
        $users = User::paginate(5);
        $roles = Role::all();
        $subject = Subject::All();
       return view('backOffice.subject.list',[
           "users" => $users,
           "roles" => $roles,
           "subjects" => $subject
       ]);
    }
    public function Add(Request $request){
        $dt = new DateTime();
        $fields = $request->get('subject');
        $fields['created_at'] = $dt->format('Y-m-d H:i:s');
        $saved = DB::table('subjects')->insertGetId($fields);
        if($saved){
            return redirect()->route('superadmin.subject.list');
        }
    }

    public function Delete($id){
        $delete = Subject::find($id);
        $deleted = $delete->delete();
        if($deleted){
            return redirect()->route('superadmin.subject.list');
        }
    }
}
