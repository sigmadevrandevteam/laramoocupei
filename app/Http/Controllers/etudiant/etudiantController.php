<?php

namespace App\Http\Controllers\etudiant;

use App\Models\Studiant;
use App\Models\Ass_course;
use App\Models\Course;
use App\Models\courseModel;
use App\models\users;
use App\User;
use App\Models\Person;
use App\Models\DegreeLevel;
use Spatie\Permission\Models\Role;
use App\Models\ServicesModels\RoleServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
class etudiantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->roleServices = new \App\Models\ServicesModels\RoleServices();
    }


    public function index()
    {

        Person::STUDIANT;

        $studiants = Person::select("people.*")
            ->where('user_type',"=", Person::STUDIANT)
            ->orderBy('id','desc')
            ->paginate(5);

        // ->orderBy('users.id','desc');
        $person_form = config('form.person');


        return view("backOffice.studiant.index",[
            "studiants"   => $studiants,
            "person_form" => $person_form["fr"],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stud = Person::STUDIANT ;


        $this->validate($request, [
            'Adresse' => 'required',
            'Ville' => 'required',
            'Nom' => 'required',
            'Prenom' => 'required',
            'Email' => 'required',
            'nationality' => 'required',
            'Code_Postal' => 'required',
            'phone_number' => 'required',
            'sexe' => 'required'
        ]);
        // save to student table
        $users = new Studiant();

        $users->adress = $request->input('Adresse');
        $users->country = $request->input('Ville');
        $users->first_name = $request->input('Nom');
        $users->last_name = $request->input('Prenom');
        $users->email = $request->input('Email');
        $users->postal_code = $request->input('Code_Postal');

        $users->save();

     $id = response()->json(['id' => $users->id]);
     $d = json_decode($id->content() , true);
            $id_stud = $d['id'];

        // save to people table
        $people = new users();

        $people->name = $request->input('Nom');
        $people->firstname = $request->input('Prenom');
        $people->email = $request->input('Email');
        $people->birthday = $request->input('birthday');
        $people->country = $request->input('Adresse');
        $people->city = $request->input('Ville');
        $people->postalcode = $request->input('Code_Postal');
        $people->nationality = $request->input('nationality');
        $people->phone_number = $request->input('phone_number');
        $people->user_type = $stud;
        $people->sex = $request->input('sexe');
        $people->role_id = $id_stud;

        $people->save();
        $message = ['message' => 'student created'];

        $person_form = config('form.person');

        //return redirect::refresh()->with('success','etudiant creer avec succèes');

       return redirect()->back()->with('success', 'Students Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $studiant = users::find($id);


/*        $studiant = Person::select("people.*","degree_levels.*","people.id as uid")
            ->leftjoin("degree_levels", "degree_levels.studiant_id", "people.id")
            ->first();
*/
        // dd($studiant->toArray() );

        $person_form = config('form.person');

        return view("backOffice.studiant.edit",[
            "studiant"    => $studiant,
            "person_form" => $person_form["fr"],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stud = Person::STUDIANT ;


        $this->validate($request, [
            'Adresse' => 'required',
            'Ville' => 'required',
            'Nom' => 'required',
            'Prenom' => 'required',
            'Email' => 'required',
            'nationality' => 'required',
            'Code_Postal' => 'required',
            'phone_number' => 'required',
            'sexe' => 'required'
        ]);


        // save to people table
        $people = users::find($id);

        $people->name = $request->input('Nom');
        $people->firstname = $request->input('Prenom');
        $people->email = $request->input('Email');
        $people->birthday = $request->input('birthday');
        $people->country = $request->input('Adresse');
        $people->city = $request->input('Ville');
        $people->postalcode = $request->input('Code_Postal');
        $people->nationality = $request->input('nationality');
        $people->phone_number = $request->input('phone_number');
        $people->user_type = $stud;
        $people->sex = $request->input('sexe');

        $role_id = $people->role_id;

        $people->save();


        $users = Studiant::find($role_id);

        $users->adress = $request->input('Adresse');
        $users->country = $request->input('Ville');
        $users->first_name = $request->input('Nom');
        $users->last_name = $request->input('Prenom');
        $users->email = $request->input('Email');
        $users->postal_code = $request->input('Code_Postal');

        $users->save();

        $person_form = config('form.person');

        return redirect("bo/admin/etudiant")->with('success',' Students updated');

      //  return redirect()->back()->with('success', 'Students Created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $people = users::find($id);

        $id_student = $people->role_id;

        $people->delete();

        $users = Studiant::find($id_student);

        $users->delete();

        return redirect()->back()->with('success', 'Students deleted');
    }

    public  function coures_list($id){

        $people = users::findOrFail($id);

        $id_student = $people->role_id;

        $stud= Studiant::find($id_student);

        $courses = Course::paginate(5);

        $assignation = Ass_course::select("student_course_list.*")
            ->where('student_id',"=", $stud->id )->get();


       $response = response()->json($assignation);
        $d = json_decode($response->content() , true);
       $id_stud = $d[0];

        return $d;


        return view('backOffice.studiant.course_list', [
            "courses" => $courses,
            "users"   => $people,
        ])->with("assignation" , $assignation);
    }
    public function assigantion($id , $id_course){

        $people = users::find($id);

        $id_student = $people->role_id;

        $stud= Studiant::find($id_student);

        $student = $stud->id;

        $course = courseModel::find($id_course);
        $course_id = $course->id;
        $ass = new Ass_course;

        $ass->student_id = $student;

        $ass->student_course = $course_id;

        $ass->save();

    return redirect()->back()->with('success', 'course affected');;
    }
    public function deassigantion($id , $id_course){

        $people = users::find($id);

        $id_student = $people->role_id;

        $stud= Studiant::find($id_student);

        $student = $stud->id;

        $course = courseModel::find($id_course);

        $course_id = $course->id;

        $ass = Ass_course;

        $ass->student_id = $student;

        $ass->student_course = $course_id;

        $ass->save();

        return redirect()->back()->with('success', 'course affected');;
    }

}
