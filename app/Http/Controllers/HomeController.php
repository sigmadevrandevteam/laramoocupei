<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $role = Role::create(['name' => 'teacher']);
        // auth()->user()->assignRole('teacher');
        
        // Role::create(["name"=> "superadmin"]);
        // $role_superadmin =  Role::findByName("superadmin");

        // auth()->user()->assignRole($role_superadmin);
        
        // auth()->user()->assignRole('superadmin');
        // exit;
        return view('frontOffice.home.index');
    }
}
