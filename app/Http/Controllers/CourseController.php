<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\VideoStream;
use App\Models\AssetsType;
use App\Models\Assets;
use Illuminate\Support\Facades\DB;
use App\Models\Course;
use App\Models\Teacher;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
         // dd('Hello word');
        return view('frontOffice.cours.index');
    }

    public function public_view()
    {
        $course_datas = \App\Models\Course::from('courses')
        
            ->leftJoin('assoc_assets_course', 'courses.id', '=', 'assoc_assets_course.id_course')
            ->leftJoin('assets',"assoc_assets_course.id_assets","=","assets.id")
            ->leftJoin('teachers',"courses.teacher_id","=","teachers.id")
            ->select('courses.*',"assets.title AS assets_title","assets.assets_fileurl","assets.assets_fileurl","assets.short_description AS assets_short_description","assets.long_description AS assets_long_description","assets.title as assets_title","assets.image_1 AS assets_image_1","assets.type_id AS assets_type_id","assets.created_at AS assets_created_at","teachers.*")
            ->get();
            
        // dd($course_datas);

        return view('frontOffice.course.index',['course_datas' => $course_datas]);
    }

public function course_details($id){
        $course_datas = DB::table('courses')
//        ->leftJoin('assoc_assets_course', 'courses.id', '=', 'assoc_assets_course.id_course')
//        ->rightJoin('assets',"assoc_assets_course.id_assets","=","assets.id")
        ->leftJoin('teachers',"courses.teacher_id","=","teachers.id")
        ->select('courses.*',"teachers.*")
        ->where('courses.id','=',$id)
        ->get();
//    "assets.title AS assets_title","assets.assets_fileurl","assets.assets_fileurl","assets.short_description AS assets_short_description","assets.long_description AS assets_long_description","assets.title as assets_title","assets.image_1 AS assets_image_1","assets.type_id AS assets_type_id","assets.created_at AS assets_created_at",
        $course_assets = DB::table('assets')
        ->leftJoin('assoc_assets_course', 'assets.id', '=', 'assoc_assets_course.id_assets')
        ->rightJoin('courses',"assoc_assets_course.id_course","=","courses.id")
        ->select("assets.title AS assets_title","assets.assets_fileurl","assets.assets_fileurl","assets.short_description AS assets_short_description","assets.long_description AS assets_long_description","assets.title as assets_title","assets.image_1 AS assets_image_1","assets.type_id AS assets_type_id","assets.created_at AS assets_created_at","assets.type_id","assets.assets_filename")
        ->where('courses.id','=',$id)
        ->get();
        $data['course_data'] = $course_datas;
        $data['assets_data'] = $course_assets;
//        dd($course_datas);
        return view('frontOffice.course.details',['course_datas' => $data]);
}
    public function news()
    {
        return view('frontOffice.news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // read video in streaming 
    public function readVideo($video_name) {
        // $dir = base_path().'/public/video/';
        // $videos_list = scandir($dir);
        // $video_name = $videos_list[$id];
        $video_path = base_path().'/public/examplevideo/'.$video_name;
        $stream = new VideoStream($video_path);
        $mimetype = "mime/type";
        header("Content-Type: ".$mimetype );
        $stream->start(); 
      }
      // end streaming
}
