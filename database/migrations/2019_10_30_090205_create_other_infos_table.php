<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDegreeLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("studiant_id")->unsigned()->nullable();
            $table->integer("test_name");
            $table->string("address")->nullable();
            $table->string("country")->nullable();
            $table->string("city")->nullable();
            $table->string("state_province")->nullable();
            $table->string("postalcode")->nullable();
            $table->longText("comment_or_questions")->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('degree_levels');
    }
}
