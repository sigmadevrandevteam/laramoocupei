<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PutSomeColumnIntoTheTablePeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people', function (Blueprint $table) {
            $table->string('nationality')->nullable();
            $table->string('phone_number')->nullable();
            $table->integer('email_application')->nullable();
            $table->string('user_name_email_application')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people', function (Blueprint $table) {
            $table->dropColumn([
                                'nationality', 
                                'phone_number', 
                                'email_application',
                                'user_name_email_application'
                            ]);
        });
    }
}
