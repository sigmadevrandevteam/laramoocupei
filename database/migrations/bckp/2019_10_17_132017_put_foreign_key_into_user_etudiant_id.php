<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PutForeignKeyIntoUserEtudiantId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //etudiant_id
            $table->bigInteger('etudiant_id')
                ->nullable(true)
                ->unsigned()
                ->after('id');
            $table->foreign('etudiant_id')
                      ->references('id')
                      ->on('etudiants');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_etudiant_id_foreign');
            $table->dropColumn('etudiant_id');
        });
    }
}
