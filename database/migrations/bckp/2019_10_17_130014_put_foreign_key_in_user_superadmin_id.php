<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PutForeignKeyInUserSuperadminId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('superadmin_id')
                ->nullable(true)
                ->unsigned()
                ->after('id');
            $table->foreign('superadmin_id')
                      ->references('id')->on('superadmins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_superadmin_id_foreign');
            $table->dropColumn('superadmin_id');
        });
    }
}
