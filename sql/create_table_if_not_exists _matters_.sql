create table if not exists `matters` 
	(`id` bigint unsigned not null auto_increment primary key, 
		`name` varchar(255) not null, 
		`description` text not null, 
		`admin_id` int null, 
		`teacher_id` int null, 
		`created_at` timestamp null, 
		`updated_at` timestamp null
	) 
	default character set utf8 collate 'utf8_unicode_ci'