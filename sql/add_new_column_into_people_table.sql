ALTER TABLE
    people 
        ADD firstname  VARCHAR(255) NOT NULL,
        ADD lastname VARCHAR(255) NOT NULL,
        ADD email VARCHAR(255) NOT NULL,
        ADD birthday DATETIME NULL,
        ADD country VARCHAR(255)  NULL,
        ADD city VARCHAR(255)  NULL,
        ADD postalcode VARCHAR(5)  NULL;

ALTER TABLE
        people 
            ADD UNIQUE `people_email_unique` (`email`) USING BTREE;