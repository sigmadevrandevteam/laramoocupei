create table `teachers` 
	(`id` bigint unsigned not null auto_increment primary key, 
		`person_id` int not null,
		`matter` int null, 
		`class` int null, 
		`created_at` timestamp null, 
		`updated_at` timestamp null) 
		default character set utf8 collate 'utf8_unicode_ci'