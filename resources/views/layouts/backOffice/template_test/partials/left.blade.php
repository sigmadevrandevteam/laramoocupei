<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset($root.'/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{ route('backOffice.dashboard') }}">
                    <i class="fa fa-th"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">Initialisation</small>
                    </span>
                </a>
            </li>
            @role('teacher')
            <li>
                <a href="{{ route('bo.prof.cours.list') }}">
                    <i class="fa fa-th"></i> <span>Course</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">0</small>
                    </span>
                </a>
            </li>
            @else

            @endrole
            @role('superadmin|admin')
            <li>
                <a href="{{ route('admin.teacher.list') }}">
                    <i class="fa fa-th"></i> <span>Teacher</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">Initialisation</small>
                    </span>
                </a>
            </li>
            @else

            @endrole
            
            @role('superadmin|admin')
            <li>
                <a href="{{ route('admin.roles.list') }}">
                    <i class="fa fa-calendar"></i> <span>Roles</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-red">3</small>
                        <small class="label pull-right bg-blue">17</small>
                    </span>
                </a>
            </li>
            @endrole
            
            <li>
                <a href="{{ route('superadmin.users.index') }}">
                    <i class="fa fa-calendar"></i> <span>Utilisateurs</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-red">3</small>
                        <small class="label pull-right bg-blue">17</small>
                    </span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>