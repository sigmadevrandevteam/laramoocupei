@extends('layouts.backOffice.adminLTE')

@section('content')
<?php
$user = auth()->user();
$id_user = $user->id;
?>
<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

@include('backOffice.users.assignment')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">User</h3>
                </div>
                <div class="box-body">
                    @role('teacher|admin|superadmin')
                    <!-- <h1>Assingation des rôles</h1> -->
                    <h1>Cours</h1>
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <form method="post" action="{{ route('superadmin.assets.addtype') }}">
                            @csrf
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" name="name" />
                                </div>
                                <div class="form-group">
                                    <label for="is_active">Enable:</label>
                                    <select class="form-control" name="is_active">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description (optional):</label>
                                    <input type="text" class="form-control" name="description" />
                                </div>
                                <input type="submit" class="btn btn-success" value="Add type" />
                            </form> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <form method="post" enctype="multipart/form-data" id="course_form" action="@if($course !=null){{ route('admin.course.updatecourse') }} @else {{ route('admin.course.savecourse') }} @endif">
                            @csrf
                            <input type="hidden" id="id_course" value="@if($course !=null){{$course->id}}@endif" name="course[id]">
                                <div class="form-group">
                                    <label for="name">Title:</label>
                                    <input value="@if($course !=null){{$course->title}}@endif" type="text" class="form-control" name="course[title]" />
                                </div>

                                <div class="form-group">
                                    <label for="name">Image:</label>
                                    <div id="img_content">
                                    @if(isset($course->image))
                                    <img src="{{asset($course->image)}}" width="500px" height="400px" />
                                    <div class="btn btn-danger" id="delete_image">Supprimer</div>
                                    @else
                                    <input type="file" class="form-control" name="image" id="image_file" />
                                    @endif
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="name">Proffesseur responsable:</label>
                                    <select name="course[teacher_id]" class="form-control">
                                        <option value="0">--Choisir--</option>
                                        @foreach($teachers as $key => $teacher)
                                        <option <?php  if((isset($teacher->id))&& isset($course->teacher_id) && $teacher->id == $course->teacher_id ){echo "selected";} ?> value="{{$teacher->id}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="name">Content:</label>
                                    <input type="text" class="form-control" value="@if($course !=null){{$course->content}}@endif" name="course[content]" />
                                </div>

                                <input type="hidden" name="course[user_id]" value="{{$id_user}}" />
                                <input type="hidden" name="course[assets_list_id]" id="assets1_id" value="@if($course !=null){{$course->assets_list_id}}@else 0 @endif">
                                <div class="form-group">
                                    <label for="is_active_assets">Ajouter support de cours:</label>
                                    <!-- <select name="is_active_assets" class="form-control" id="is_active_assets">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select> -->
                                    <div id="form_assets_on" data-fancybox="" data-animation-duration="500" data-src="#form_assets"id="add_assets_btn" class="btn btn-info" >+</div>
                                    <div class="row">
                                        <div id="all_assets_content">
                                        @if($assets_list != null)
                                        @foreach($assets_list as $key => $assets_associated)                                       
                                       <div class='col-lg-12'>{{$assets_associated->title}}</div><div class='col-lg-4'></div>
                                        @endforeach
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <?php /* <div class="form-group" id="support_content" style="display:none;">
                                    <label for="matiere_id">Matière:</label>
                                    <select class="form-control" name="matiere_id" id="matiere_id">
                                        <option value="null">Choose</option>
                                        @foreach($matiere_list as $key => $matiere)
                                        <option value="{{$matiere->id}}">{{$matiere->name}}</option>
                                        @endforeach
                                    </select>
                                </div> */ ?>

                                <?php /* <div class="form-group" id="support_content" style="display:none;">
                                    <label for="matiere_id">Teacher:</label>
                                    <select class="form-control" name="teacher_id" id="teacher_id">
                                        <option value="null">Choose</option>
                                        @foreach($teacher_list as $key => $teacher)
                                        <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </div> */ ?>

                                <div class="form-group">
                                    <label for="short_desc">Description courte (optionel):</label>
                                    <input type="text" value="@if($course !=null)
                                    {{$course->short_desc}}
                                    @endif
                                    " class="form-control" name="course[short_desc]" />
                                </div>

                                <div class="form-group">
                                    <label for="long_desc">Description longue (optionel):</label>
                                    <textarea class="form-control" name="course[long_desc]" >
                                    @if($course !=null){{$course->long_desc}}@else  @endif
                                    </textarea>
                                </div>

                                <div class="form-group">
                                    <label for="date_debut">Date de début:</label>
                                    <input type="date" class="form-control" value="@if($course !=null){{$course->date_debut}}@else  @endif" name="course[date_debut]" />
                                </div>

                                <div class="form-group">
                                    <label for="date_fin">Date de fin:</label>
                                    <input type="date" class="form-control" value="@if($course !=null){{$course->date_fin}}@else  @endif" name="course[date_fin]" />
                                </div>

                                <input type="submit" class="btn btn-success" value="Save course" />

                            </form>
                            <div class="form-group" style="display:none;" id="form_assets">

                                <form method="POST" enctype="multipart/form-data" id="submit_assets" action="javascript:void(0)">
                                <div class="form-group" id="support_content" style="">
                                    <label for="is_active">Type de support:</label>
                                    <select class="form-control" name="is_active" id="type_assets">
                                        <option value="null">Choose</option>
                                        @foreach($assets_type as $key => $assettype)
                                        <option value="{{$assettype->id}}">{{$assettype->name}}</option>
                                        @endforeach
                                    </select>
                                </div>    
                                <label for="title">Titre (requis)</label>
                                    <input id="title" name="title" type="text" class="form-control" />
                                    <label for="assets_fileurl">Upload fichier (required):</label>
                                    <input id="assets_fileurl" type="file" class="form-control" name="assets_fileurl" />
                                    <label for="assets_fileurl">Upload Image:</label>
                                    <input id="image1" type="file" class="form-control" name="image1" />
                                    <img id="image_preview_container" src="{{ asset('public/image/image-preview.png') }}" alt="preview image" style="max-height: 150px;">
                                    <input id="assets_user_id" type="hidden" name="assets_user_id" value="{{$id_user}}">
                                    <label for="short_desc">description courte</label>
                                    <input id="short_desc" class="form-control" type="text" nam="short_desc">
                                    <label for="long_desc">description longue</label>
                                    <textarea id="long_desc" class="form-control" nam="long_desc">
                                    </textarea>
                                    <div class="text-center" id="submit_container">
                                        <span id="error_block_assets" style="color:red;display:none;">Champ obligatoire non renseigné</span>
                                        <input class='btn btn-info' id='save_assets' value='Enregistrer' type='submit'>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    @else
                    <h1>
                        No record found
                    </h1>
                    @endrole
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('javascript')

<script type="text/javascript">
    $(document).on('click', '#new-assignment', function(event) {
        event.preventDefault();
        var user_to_assigned = $(this).data('id');
        $('#modal-assignment').modal('show');
        $('#user_to_assigned').val(user_to_assigned);

        $.ajax({
            "url": $(this).data('url'),
            'method': "GET",
            "dataType": "json",
            success: function(user_roles) {

                $('#user_assignment').find('input').each(function($item, $key) {

                    $($key).removeAttr('checked');

                    for (i = 0; i < user_roles.length; i++) {
                        if (user_roles[i] == $($key).data('name')) {

                            $($key).attr('checked', true)
                        }
                    }

                })
                // alert(response)
            }
        });

    });
    $(document).ready(function() {
        var defaultNbAssets = 0;
        $("#add_assets_btn").click(function() {

            
            defaultNbAssets++;
        });

        $("#is_active_assets").change(function() {
            if ($("#is_active_assets").val() === "1") {
                $("#support_content").css("display", "block");
            }
        });

        $('#image1').change(function() {
            let reader = new FileReader();
            reader.onload = (e) => {
                $('#image_preview_container').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        });

        $("#submit_assets").submit(function(e) {
            e.preventDefault();
            title = $('#title').val();
            if ((title != "" && title != null && title != undefined) && ($("#assets_fileurl").val() != "" && $("#assets_fileurl").val() != null && $("#assets_fileurl").val() != undefined)) {
                var assets_contents =$("#all_assets_content").html()
                $("#submit_container").html("<img src='{{ url('/') }}/images/load_lanc.gif'>");
                type_id = $('#type_assets').val();
                short_desc = $('#short_desc').val();
                long_desc = $('#long_desc').val();
                assets_user_id = $('#assets_user_id').val();
                var formData = new FormData(this);
                var token = $('#token').attr('content');
                formData.append('type_id', type_id);
                formData.append('title', title);
                formData.append('short_desc', short_desc);
                formData.append('long_desc', long_desc);
                formData.append('assets_user_id', assets_user_id);
                formData.append('_token', token);

                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.assets.save') }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function(data) {
                        $("#assets1_id").val(data.asset_id);
                        $("#all_assets_content").html(assets_contents+"<div class='col-lg-12'>"+data.titre+"</div><div class='col-lg-4'></div>");                        
                        $("#submit_container").html("<input class='btn btn-info' id='save_assets' value='Enregistrer' type='submit'>");
                        $(".fancybox-close-small").click();
                    },error:function(error){
                        console.log(error)
                    }
                });
            }else {
                $("#error_block_assets").css("display", "block");
            }
        });
        $("#delete_image").click(function(){
            alert("okok");
            var id_course = $("#id_course").val();
            var formData = new FormData();
            var token = $('#token').attr('content');
            formData.append('id_course', id_course);
            formData.append('_token', token);
            
            $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.course.delete_image') }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function(data) {
                        $("#assets1_id").val(data.asset_id);
                        $("#img_content").html("<input type='file' class='form-control' name='image' id='image_file' />");                        
                    },error:function(error){
                        console.log(error)
                    }
                });
        });
    });
</script>

@stop