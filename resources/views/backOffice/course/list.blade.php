@extends('layouts.backOffice.adminLTE')

@section('content')
<?php
    $user = auth()->user();                           
    $id_user = $user->id;
?>
<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

@include('backOffice.users.assignment')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">User</h3>
                </div>
                <div class="box-body">
                    @role('teacher|admin|superadmin')
                    <!-- <h1>Assingation des rôles</h1> -->
                    <h1>Cours</h1>
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <form method="post" action="{{ route('superadmin.assets.addtype') }}">
                            @csrf
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" name="name" />
                                </div>
                                <div class="form-group">
                                    <label for="is_active">Enable:</label>
                                    <select class="form-control" name="is_active">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description (optional):</label>
                                    <input type="text" class="form-control" name="description" />
                                </div>
                                <input type="submit" class="btn btn-success" value="Add type" />
                            </form> -->
                        <a href="{{ route('admin.course.add') }}"><button class="btn btn-success">Ajouter cours</button></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            id</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Titre</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Déscription</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Active</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Actions</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($course_lists as $key => $cours)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $cours->id }}</td>
                                        <td>{{ $cours->title }}</td>
                                        <td>
                                            {{ $cours->short_desc }}
                                        </td>
                                        <td>
                                        <?php if($cours->is_active == 1){  ?> 
                                            <span style="color:green">Activé</span>
                                        <?php } else{ ?> 
                                            <span style="color:red">Désactivé</span>
                                        <?php } ?>                                    </td>
                                        <td>
                                        <a  href="{{ route('admin.course.updateForm',$cours->id) }}"><button class="btn btn-info">Modifier</button></a>
                                            <a  href="{{ route('admin.course.delete',$cours->id) }}"><button class="btn btn-danger">Supprimer</button></a>
                                        </td>
                                    </tr>
                                    @endforeach

                            
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                    <h1>
                        No record found
                    </h1>
                    @endrole
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('javascript')

<script type="text/javascript">
    $(document).on('click', '#new-assignment', function(event) {
        event.preventDefault();
        var user_to_assigned = $(this).data('id');
        $('#modal-assignment').modal('show');
        $('#user_to_assigned').val(user_to_assigned);

        $.ajax({
            "url": $(this).data('url'),
            'method': "GET",
            "dataType": "json",
            success: function(user_roles) {

                $('#user_assignment').find('input').each(function($item, $key) {

                    $($key).removeAttr('checked');

                    for (i = 0; i < user_roles.length; i++) {
                        if (user_roles[i] == $($key).data('name')) {

                            $($key).attr('checked', true)
                        }
                    }

                })
                // alert(response)
            }
        });

    });
</script>

@stop