
@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        @if ($is_empty === true) 
            <div class="col-md-12">
                <h3 class="alert alert-warning">L'administrateur vous a pas encore attribué aucune matière !</h3>
            </div>
        @endif
        <div class="col-md-6">
            <div class="box">

                <div class="box-header">
                  <h2 class="box-title">Nouveau cours</h2>
                </div>
                    
                <!-- /.box-header -->
                <div class="box-body">

                    @if ($is_empty === true) 
                        
                      
                        @else
                        <form class="form-horizontal" action="{{ route('bo.prof.course.store') }}" method="post">
                            
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-md-12 form-group">

                                <select name="course[matter_id]" class="form-control">
                                    <option>---</option>
                                    @foreach($matters as $key => $matter)
                                        <option value="{{ $matter->id }}">{{ $matter->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label>Title</label>
                                <input name="course[title]" class="form-control">
                            </div>

                            <div class="col-md-12 form-group" >
                                <label>Contenu</label>
                                <textarea style="max-width: 100%;min-width: 100%;min-height: 200px;" class="form-control" name="course[content]"></textarea>
                            </div>

                            <button value="submit" class="btn btn-info">Enregister</button>                        
                        </form>
                    @endif
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>

@stop

@section('javascript')

<!-- <script src="{{ asset('js/page.js') }}"></script> -->
<script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
<script type="text/javascript">


$('#search-page').script();

/**
--------------------------------------------------------------------------------


/**
---------------------------------------------------------------------------------
 */
//Date range picker
$('#reservation').daterangepicker()
    //Date range picker with time picker
$('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        format: 'MM/DD/YYYY h:mm A'
    })



//Date picker
$('#datepicker').datepicker({
    autoclose: true
})

</script>
@stop