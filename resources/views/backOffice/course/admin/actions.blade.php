
@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }} / Cours / Actions # {{ $id }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                  <h3 class="box-title">Assignation d'un cours </h3>
                </div>
                    
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h2> Cours :  {{ $course->title }} </h2>
                             <form action="{{ route('bo.admin.course.save_actions') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="assignment_course[course_id]" value="{{ $id }}">
                               
                                    
                                        @foreach ($teachers as $key => $teacher)

                                        <div class="form-control">
                                            <label for="{{ $teacher->uid }}""> {{ $teacher->name }}</label>
                                            <input id="{{ $teacher->uid }}" type="radio" name="assignment_course[user_id]" value="{{ $teacher->uid }}">
                                        </div>

                                        @endforeach

                                    <br>
                                    <div class="form-group">
                                        <button class="btn btn-info" type="submit">Enregistrer</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection