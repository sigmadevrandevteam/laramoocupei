
@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

@include('backOffice.users.new')
@include('backOffice.users.assignment')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

            <div class="box-header">
              <h3 class="box-title">User</h3>
              <button class="btn btn-info" id="new-user"> <i class="fa fa-plus"></i></button>
            </div>
                <div class="box-body">
                    @role('superadmin|admin')
                    <!-- <h1>Assingation des rôles</h1> -->
                    <h1>Assinging roles</h1>
                    
                    <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            id</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">nom</th>
                                            <th>Personne</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Role assigned</th>
                                            <th>Connecter</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Actions</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($users as $key => $user)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $people }}</td>
                                                <td>

                                                    <?php
                                                        $items = \Spatie\Permission\Models\Role::all();
                                                            
                                                        $hasRoles =[];

                                                            $items->each(function ($item, $key) use(&$hasRoles, $user){

                                                                if ($user->hasrole($item->name))
                                                                {
                                                                    array_push($hasRoles, $item->name);
                                                                }
                                                                
                                                            });
                                                            if (!empty($hasRoles))
                                                            {

                                                                echo '<span class="badge badge-danger">'.implode(" | ", $hasRoles).'</span>';
                                                            }else
                                                                {
                                                                    echo '<span>Not assigned</span>';
                                                                }
                                                            $hasRoles =[];
                                                    ?>
                                                    
                                                </td>
                                                <td>
                                                    @if($user->is_connected == 1)
                                                    <small class="label pull-right bg-green">Connecté</small>
                                                    @endif
                                                </td>
                                                <td>
                                                    
                                                    <a id="new-assignment" data-url="{{ route('get_list_user_role', $user->id)}}" href="#" data-id="{{ $user->id }}"><i class="fa fa-plus"></i> Assingin</a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('user_profile', $user->id) }}">Show</a>
                                                </td>
                                             
                                            </tr>
                                        @endforeach

                                        {{ $users->links() }}
                                    </tbody>
                                </table>
                            </div>
                    </div>
                    @else
                    <h1>
                        I can't assinging roles
                    </h1>
                    @endrole
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('javascript')

<script type="text/javascript">
    

    $(document).on('click', "#new-user", function(event){
        event.preventDefault();
        $('#modal-user').modal('show');
    });

    $("#birthday").datepicker({ 
        autoclose: true,
        inline: true,
        format: "yyyy/mm/dd",
        // container: "#birthDayContainer",
        autoPick: true,
        zIndex: 1062,
        left:0
    });



    $(document).on('click', '#new-assignment', function(event){
        event.preventDefault();
        var user_to_assigned =  $(this).data('id');
        $('#modal-assignment').modal('show');
        $('#user_to_assigned').val(user_to_assigned);

        $.ajax({
            "url"       : $(this).data('url'),
            'method'    : "GET",
            "dataType"  : "json",
            success     : function(user_roles)
            {
                
                $('#user_assignment').find('input').each(function($item, $key) {
                        
                       $($key).removeAttr('checked');

                       for(i =0; i< user_roles.length; i++)
                       {
                            if (user_roles[i] == $($key).data('name')) {
                                
                                $($key).attr('checked', true)
                            }
                       }
                    
                })
                // alert(response)
            }
        });

    });
</script>

@stop