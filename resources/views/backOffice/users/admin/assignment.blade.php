<!-- /.box-header -->
<div class="modal fade" id="modal-assignment">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{ route('admin.role.assignment') }}">
            <!-- form -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-shadow">New role assignment</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->

                        {{ csrf_field() }}

                        <input id="user_to_assigned" type="hidden" name="user_to_assigned">
                        
                        <div class="box-body">
                            <div class="premary-content">
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="select">
                                            <div class="form-group">
                                                <div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div class="row content-list">
                                    <div class="col-md-7">
                                        
                                        <table class="table table-bordered table-hover dataTable">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                    id</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">nom</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="user_assignment">
                                                @foreach($roles as $key => $role)
                                                @if ($role->name !== 'superadmin' )
                                                    <tr role="row" class="odd">
                                                      <td class="sorting_1">{{ $role->id }}</td>
                                                      <td>{{ $role->name }}</td>
                                                      <td>
                                                        <input type="checkbox"  class="{{$role->name}}" data-name="{{$role->name}}" name="role[{{ $role->id }}]" value="{{ $role->id }}">
                                                        </td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                                <!--  -->
                            </div>
                        </div>
                    <!-- /.box-body -->


                <!-- end modal-body -->
                </div>
                <div class="modal-footer">
                    
                    <button type="submit" class="btn btn-primary" id="">Enregistrer</button>
                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
                </div>
            <!-- /form -->
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{{--
    @section('javascript')

<script>


    $(document).ready(function(){

        //Initialize Select2 Elements
    })
</script>

@stop

--}}