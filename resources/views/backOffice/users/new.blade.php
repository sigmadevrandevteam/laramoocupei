<!-- /.box-header -->
<div class="modal fade" id="modal-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{ route('admin.user.create') }}">
            <!-- form -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-shadow">{{ ucfirst($person_form['page_title']) }}</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->

                        {{ csrf_field() }}
                        
                        <div class="box-body">
                            <div class="premary-content">
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="select">
                                            <div class="form-group">
                                                <div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div class="row content-list">

                                    <div class="col-md-6">
                                        <h2>{{ ucfirst($person_form['personal_infos']) }}</h2>
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['name']) }}</label>
                                            <input value="{{ config('users.default.name') }}" type="text" name="person[name]" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['password']) }}</label>
                                            <input value="{{ config('users.default.password') }}" type="text" name="person[password]" class="form-control">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['email']) }}</label>
                                            <select name="person[people_email]" class="form-control">
                                             
                                                @foreach($people as $person)
                                                    <option value="{{ $person->id }}">{{ $person->email }} - {{ config('datamiror.people')[$person->user_type]  }}</option>
                                                @endforeach
                                            </select>
                                        </div> 

                                    </div>

                                    <div class="col-md-6">

                                        
                                    </div>

                                </div>

                                <!--  -->
                            </div>
                        </div>
                    <!-- /.box-body -->


                <!-- end modal-body -->
                </div>
                <div class="modal-footer">
                    @if(count($people)>0)
                    
                    <button type="submit" class="btn btn-primary" id="">Enregistrer</button>
                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
                    @else
                    <button type="submit" class="btn btn-primary" disabled>Enregistrer</button>
                    @endif
                </div>
            <!-- /form -->
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
<style>
.radio {
  margin: 16px 0;
  display: block;
  cursor: pointer;
}
.radio input {
  display: none;
}
.radio input + span {
  line-height: 22px;
  height: 22px;
  padding-left: 22px;
  display: block;
  position: relative;
}
.radio input + span:not(:empty) {
  padding-left: 30px;
}
.radio input + span:before, .radio input + span:after {
  content: '';
  width: 22px;
  height: 22px;
  display: block;
  border-radius: 50%;
  left: 0;
  top: 0;
  position: absolute;
}
.radio input + span:before {
  background: #D1D7E3;
  transition: background 0.2s ease, transform 0.4s cubic-bezier(0.175, 0.885, 0.32, 2);
}
.radio input + span:after {
  background: #fff;
  transform: scale(0.78);
  transition: transform 0.6s cubic-bezier(0.175, 0.885, 0.32, 1.4);
}
.radio input:checked + span:before {
  transform: scale(1.04);
  background: #5D9BFB;
}
.radio input:checked + span:after {
  transform: scale(0.4);
  transition: transform 0.3s ease;
}
.radio:hover input + span:before {
  transform: scale(0.92);
}
.radio:hover input + span:after {
  transform: scale(0.74);
}
.radio:hover input:checked + span:after {
  transform: scale(0.4);
}

.radio-inline .radio {
    /* float:left; */
}
.textarea {
    max-width: 100%;
    height: 100%;
    min-height:205px;
}

</style>