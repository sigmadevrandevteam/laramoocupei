 <!-- /.box-header -->
<div class="modal fade" id="modal-teacher">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['action' => 'BackOffice\TeacherController@store','method'=>'POST']) !!}
            <!-- form -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-shadow">New teacher</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="premary-content">
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="select">
                                            <div class="form-group">
                                                <div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div class="row content-list">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['name']),['for'=> 'Nom'])}}
                                            {{Form::text('Nom', '', ['class' => 'form-control', 'placeholder' => 'Entrer le Nom du proffeseur','required'])}}
                                        </div>


                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['lname']),['for'=> 'Prenom'])}}
                                            {{Form::text('Prenom', '', ['class' => 'form-control', 'placeholder' => 'entrer le Prenom du proffeseur','required'])}}
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['email']),['for'=> 'Email'])}}
                                            {{Form::email('Email', '', ['class' => 'form-control', 'placeholder' => 'Entrer de Email du proffeseur','required'])}}
                                        </div>


                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['nationality']),['for'=> 'nationality'])}}
                                            {{Form::text('nationality', '', ['class' => 'form-control', 'placeholder' => 'Entrer la nationalite du proffeseur','required'])}}
                                        </div>


                                        <div class="form-group">


                                            {{Form::label('title', ucfirst($person_form['birthday']),['for'=> 'birthday'])}}
                                            {{Form::date('birthday', \Carbon\Carbon::now(), ['class' => 'form-control','required'])}}

                                            <div style="position: relative; ">
                                                <div id="birthDayContainer" style="position: absolute;"></div>
                                            </div>



                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['sex']['label']), ['for'=> 'sex'])}}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="radio">
                                                        {{Form::radio('sexe', 1 , true, ['id' => 'sex-homme'])}}
                                                        <span>{{ ucfirst($person_form['sex']['mal']) }}</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="radio">
                                                        {{Form::radio('sexe', 2 , ['id' => 'sex-femme'])}}
                                                        <span>{{ ucfirst($person_form['sex']['femal']) }}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['city']),['for'=> 'Ville'])}}
                                            {{Form::text('Ville', '', ['class' => 'form-control', 'placeholder' => 'Entrer le nom du ville du Professeur','required'])}}
                                        </div>


                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['phone_number']),['for'=> 'phone_number'])}}
                                            {{Form::number('phone_number', '', ['class' => 'form-control','placeholder' => 'Entrer le numero de telephone du Professeur' ,'required'])}}
                                        </div>

                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label for="content">{!! ucfirst($person_form['teacher']['matter']) !!}</label>
                                            <select class="form-control" name="teacher[matter_id]">

                                                @foreach (config('datamiror.matters') as $key => $matter)
                                                    <option value="{{ $key }}">{{ $matter }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{!! ucfirst($person_form['teacher']['class']) !!}</label>
                                            <select class="form-control" name="teacher[class_id]">
                                                {{-- <option value="1"> 1 ère anné</option> --}}
                                                @foreach (config('datamiror.class') as $key => $class)
                                                    <option value="{{ $key }}">{{ $class }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <!--  -->
                            </div>
                        </div>
                    <!-- /.box-body -->


                <!-- end modal-body -->
                </div>
                <div class="modal-footer">

                {{Form::submit('Enregistrer', ['class' =>'btn btn-block btn-lg btn-primary' ])}}
                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
                </div>
            <!-- /form -->
                {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{{--
    @section('javascript')

<script>


    $(document).ready(function(){

        //Initialize Select2 Elements
    })
</script>

@stop

--}}
