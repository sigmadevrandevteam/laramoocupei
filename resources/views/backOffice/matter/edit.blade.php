
@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">

                <div class="box-header">
                  <h3 class="box-title">Nouveau cours</h3>
                </div>
                    
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['action' => ['BackOffice\Admin\MatterAdminController@update' , $matter->id],'method'=>'POST']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-md-12 form-group">

                    </div>

                    <div class="col-md-12 form-group">
                        {{Form::label('title', 'Title',['for'=> 'Nom'])}}
                        {{Form::text('title', $matter->name, ['class' => 'form-control', 'placeholder' => 'entrer le titre du matiere','required'])}}
                    </div>

                    <div class="col-md-12 form-group" >
                        <label>Description</label>
                        {{Form::textarea('description',  $matter->description , ['class' => 'form-control', 'placeholder' => 'entrer un description pour la matiere du matiere','required'])}}
                    </div>

                {{Form::submit('Enregistrer', ['class' =>'btn btn-block btn-lg btn-primary' ])}}


                {!! Form::close() !!}


                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>

@stop

@section('javascript')

<!-- <script src="{{ asset('js/page.js') }}"></script> -->
<script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
<script type="text/javascript">


$('#search-page').script();

/**
--------------------------------------------------------------------------------


/**
---------------------------------------------------------------------------------
 */
//Date range picker
$('#reservation').daterangepicker()
    //Date range picker with time picker
$('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        format: 'MM/DD/YYYY h:mm A'
    })



//Date picker
$('#datepicker').datepicker({
    autoclose: true
})

</script>
@stop