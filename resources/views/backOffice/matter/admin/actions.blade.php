
@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">

                <div class="box-header">
                  <h3 class="box-title">Attribution d'un matière</h3>

                    @if($assigned)
                        <span class="badge badge-primary">Assigné</span>
                        @else
                        <span class="badge">Non</span>
                        @endif
                 
                </div>
                    
                <!-- /.box-header -->
                <div class="box-body">
                    <form class="form-horizontal" action="{{ route('bo.prof.matter.assigner', $id) }}" method="post">
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-md-12 form-group">
                            
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Enseignant</label>
                            <select name="matter_assignment[teacher]" class="form-control">
                                @foreach ($teachers as $teacher)
                                    <option value="{{ $teacher->id }}">{{ $teacher->name." ".$teacher->firstname }}</option>
                                @endforeach
                            </select>
                            
                        </div>

                        <div class="col-md-12 form-group">
                            <label>Action</label>
                            <select name="matter_assignment[action]" class="form-control">
                                @if ($assigned)
                                    <option value="1" selected="true">Assigner</option>
                                    <option value="2">Désactiver</option>
                                    @else
                                    <option value="1">Assigner</option>
                                    <option value="2" selected="true">Désactiver</option>
                                @endif
                                
                            </select>
                            
                        </div>



                        <input type="hidden" name="matter_assignment[id]" value="{{ $id }}">

                        <button value="submit" class="btn btn-info">Eregistrer</button>
                        

                    </form>
                    
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>

@stop

@section('javascript')

<!-- <script src="{{ asset('js/page.js') }}"></script> -->
<script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
<script type="text/javascript">


$('#search-page').script();

/**
--------------------------------------------------------------------------------


/**
---------------------------------------------------------------------------------
 */
//Date range picker
$('#reservation').daterangepicker()
    //Date range picker with time picker
$('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        format: 'MM/DD/YYYY h:mm A'
    })



//Date picker
$('#datepicker').datepicker({
    autoclose: true
})

</script>
@stop