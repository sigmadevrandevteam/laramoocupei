


@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                  <h3 class="box-title">Matières</h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                        <div class="row">
                            <div class="col-sm-12">
                                <a class="btn btn-info" href="{{ route('bo.prof.matter.new') }}">Nouvelle matière</a>
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            id</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">nom</th>
                                            <th>Assignation</th>
                                            <th colspan="2"> Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($matters))

                                            @foreach($matters as $key => $matter)
                                                <tr role="row" class="odd">
                                                    <td class="sorting_1">{{ $matter->id }}</td>
                                                    <td>{{ $matter->name }}</td>
                                                    <td>
                                                        @if($matter->teacher_id)
                                                        <span class="badge badge-primary">Assigné</span>
                                                        @else
                                                        <span class="badge">Non</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-warning" href="{{ route('bo.prof.matter.edit', $matter->id) }}">Edit</a>

                                                        <a onClick="return confirm('Vous-êtes sur?')" class="btn btn-danger" href="{{ route('bo.prof.matter.destroy', $matter->id) }}">Supprimer</a>
                                                    </td>

                                                    <td>
                                                        <a class="btn btn-info" href="{{ route('bo.prof.matter.actions', $matter->id) }}"> Assignation</a>

                                                    </td>
                                                </tr>
                                            @endforeach


                                            @if (isset($matter) && $matter->count() > 5)

                                                <tr>
                                                    <td colspan="3" style="text-align: center;">
                                                        {{ $matters->links() }}
                                                    </td>
                                                </tr>
                                            @endif

                                            @if (!isset($matter))
                                                <tr>
                                                    <td colspan="3" style="text-align: center;">
                                                        <h2>Vous n'avez pas encore ajouter de matière</h2>
                                                    </td>
                                                </tr>
                                            @endif

                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>

@stop

@section('javascript')

<!-- <script src="{{ asset('js/page.js') }}"></script> -->
<script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
<script type="text/javascript">


$('#search-page').script();

/**
--------------------------------------------------------------------------------


/**
---------------------------------------------------------------------------------
 */
//Date range picker
$('#reservation').daterangepicker()
    //Date range picker with time picker
$('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        format: 'MM/DD/YYYY h:mm A'
    })


//Date picker
$('#datepicker').datepicker({
    autoclose: true
})

</script>
@stop
