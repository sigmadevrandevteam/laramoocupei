
@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h2 class="box-title"> 
                        <span style="color:red;">
                            Supprimer la matières 
                        </span>
                    </h2>
                </div>
                    
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="{{ route('bo.prof.matter.delete.confirmed', $matter->id) }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <h3>- {{ $matter->name }}</h3>
                                    <input type="hidden" name="matter" value="{{ $matter->id }}">
                                    
                                    <ul class="list-li"> 
                                        @foreach($courses as $key => $course)
                                            <li>{{ $course->title }}

                                                <a href="{{ route('bo.prof.matter.course_destroy', 
                                                ['id' => $course->id,'matter_id'=>$matter->id])
                                                 }}">Supprimer</a>
                                            </li>
                                            <input type="hidden" name="courses[{{ $course->id }}]" value="{{ $course->id }}">


                                        @endforeach
                                    </ul>
                                    <p class="alert alert-warning">
                                        Est-vous sur de vouloir supprimer la matière, ainsi que les 
                                        cours qui sont directement à ce matière 
                                    </p>
                                <button value="submit" class="btn btn-danger">Supprimer</button>
                                </form>
                            </div>


                        </div>
                    </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>

@stop

@section('javascript')

<!-- <script src="{{ asset('js/page.js') }}"></script> -->
<script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
<script type="text/javascript">


</script>
@stop