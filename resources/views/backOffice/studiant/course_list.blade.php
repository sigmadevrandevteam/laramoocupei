
@extends('layouts.backOffice.adminLTE')

@section('content')

    <section class="content-header">
        <h1>
            {{ config('app.name') }}
            <small>Recherche</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            {{--
                <li><a href="#">Forms</a></li>
            --}}
            <li class="active">home</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="box-header">
                        <h3 class="box-title">assigner un cours a {{ $users->name }}</h3>
                    </div>
                        <div class=" block block-inline"><?echo //$assigned[0] ?></div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                id</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">nom</th>
                                            <th colspan="2"> Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($courses as $key => $course)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{ $course->id }}</td>
                                                <td>{{ $course->title }}</td>
                                                <td>
                                                    @if($course->id == 0)
                                                    <a
                                                        onClick="return confirm('assigner ce cours a cette eleve')"
                                                        class="btn btn-primary"
                                                        href="/bo/admin/etudiant/assign/{{$users->id}}/{{$course->id }}">assigner</a>
                                                    @else
                                                        <a
                                                            onClick="return confirm('supprimer l\'assignation a ce cours ?')"
                                                            class="btn btn-primary "
                                                            href="/bo/admin/etudiant/deassign/{{$users->id}}/{{$course->id }}">supprimer</a>
                                                        @endif



                                                </td>
                                            </tr>
                                        @endforeach

                                        @if ($course->count() > 5)
                                            <tr>
                                                <td colspan="3" style="text-align: center;">
                                                    {{ $courses->links() }}
                                                </td>
                                            </tr>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

@stop

@section('javascript')

    <!-- <script src="{{ asset('js/page.js') }}"></script> -->
    <script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
    <script type="text/javascript">


        $('#search-page').script();

        /**
         --------------------------------------------------------------------------------


         /**
         ---------------------------------------------------------------------------------
         */
//Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })

        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function(start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

    </script>
@stop
