
@extends('layouts.backOffice.adminLTE')
@push('stylesheet')
  <link href=" {{ asset('css/form-radio.css') }} " rel="stylesheet" />
@endpush
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
			{!! $errors->first('email', \App\Helpers\Alert::show(':message','danger')) !!}
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Studiants</h3>

                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['action' => ['etudiant\etudiantController@update' , $studiant->id],'method'=>'POST']) !!}
		            <!-- form -->

		                    <!-- form start -->

		                        {{ csrf_field() }}

		                        <div class="box-body">
		                            <div class="premary-content">
		                                <!--  -->
		                                <div class="row">
		                                    <div class="col-md-12">
		                                        <div class="select">
		                                            <div class="form-group">
		                                                <div>

		                                                </div>
		                                            </div>
		                                        </div>

		                                    </div>
		                                </div>
		                                <!--  -->
		                                <!--  -->
		                                <div class="row content-list">

		                                    <div class="col-md-6">
		                                    	<h2>{{ ucfirst($person_form['personal_infos']) }}</h2>

		                                    	<input type="hidden" name="person[id]" class="form-control" value="{{ $studiant->uid }} ">

                                                <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['name']),['for'=> 'Nom'])}}
                                                    {{Form::text('Nom', $studiant->name, ['class' => 'form-control', 'placeholder' => 'Entrer le Nom de l\'éleve','required'])}}
                                                </div>

                                                <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['lname']),['for'=> 'Prenom'])}}
                                                    {{Form::text('Prenom',$studiant->firstname , ['class' => 'form-control', 'placeholder' => 'entrer le Prenom de l\'éleve','required'])}}
                                                </div>

                                                <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['email']),['for'=> 'Email'])}}
                                                    {{Form::email('Email', $studiant->email, ['class' => 'form-control', 'placeholder' => 'Entrer de Email de l\'éleve','required'])}}
                                                </div>

		                                        <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['birthday']),['for'=> 'birthday'])}}
		                                            <input id="birthday" type="" name="person[birthday]" class="form-control" value=" {{ $studiant->birthday }} " required>

		                                            <div style="position: relative; ">
		                                                <div id="birthDayContainer" style="position: absolute;"></div>
		                                            </div>

		                                        </div>
		                                       	<div class="form-group">
		                                            <label for="content">{{ ucfirst($person_form['sex']['label']) }}</label>
		                                            <div class="row">
		                                                <div class="col-md-6" id="sex_radio" data-sex="{{ $studiant->sex }}">
		                                                    <label class="radio">
		                                                        <input id="sex-homme" type="radio" name="sexe"  value="1" >
		                                                        <span>{{ ucfirst($person_form['sex']['mal']) }}</span>
		                                                    </label>
		                                                </div>
		                                                <div class="col-md-6">
		                                                    <label class="radio">
		                                                        <input id="sex-femme" type="radio" name="sexe" class="" value="2">
		                                                        <span>{{ ucfirst($person_form['sex']['femal']) }}</span>
		                                                    </label>
		                                                </div>
		                                            </div>
		                                        </div>


		                                    </div>
		                                    <div class="col-md-6">
		                                        <h2>{{ $person_form['degree_level']['title_section'] }}</h2>
                                                <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['degree_level']['address']),['for'=> 'Adresse'])}}
                                                    {{Form::text('Adresse', $studiant->country, ['class' => 'form-control', 'placeholder' => 'Entrer de Adresse de l\'éleve','required'])}}
                                                </div>

                                         <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['city']),['for'=> 'Ville'])}}
                                                    {{Form::text('Ville', $studiant['city'], ['class' => 'form-control', 'placeholder' => 'Entrer le nom du ville de l\'éleve','required'])}}
                                         </div>

                                                <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['nationality']),['for'=> 'nationality'])}}
                                                    {{Form::text('nationality', $studiant->nationality, ['class' => 'form-control', 'placeholder' => 'Entrer la nationalite de l\'éleve','required'])}}
                                                </div>


                                                <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['degree_level']['postal_code']),['for'=> 'Code Postal'])}}
                                                    {{Form::number('Code_Postal', $studiant['postalcode'], ['class' => 'form-control','placeholder' => 'Entrer le code postal de l\'éleve' ,'required'])}}
                                                </div>

                                                <div class="form-group">
                                                    {{Form::label('title', ucfirst($person_form['phone_number']),['for'=> 'phone_number'])}}
                                                    {{Form::number('phone_number', $studiant->phone_number, ['class' => 'form-control','placeholder' => 'Entrer le numero de telephone de l\'éleve' ,'required'])}}
                                                </div>


		                                    </div>
		                                </div>

		                                <!--  -->
		                            </div>
		                        </div>
		                    <!-- /.box-body -->
		                <!-- end modal-body -->
		                </div>
		                <div class="modal-footer">
                        {{Form::hidden('_method', 'PUT')}}
                        {{Form::submit('Validate', ['class' =>'btn btn-block btn-lg btn-primary' ])}}

		                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
		                </div>
		            <!-- /form -->
                {!! Form::close() !!}
				</div>

			</div>
		</div>
	</div>
</section>

@stop

@section("javascript")

<script type="text/javascript">


	var sex = $('#sex_radio').data("sex");

	if (sex == 1)
	{
		 $("#sex-homme").prop("checked", true);
	}else
		if (sex == 2)
		{
			 $("#sex-femme").prop("checked", true);
		}


</script>

@endsection
