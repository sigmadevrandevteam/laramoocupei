<!-- /.box-header -->
<div class="modal fade" id="modal-studiant">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['action' => 'etudiant\etudiantController@store','method'=>'POST']) !!}
            <!-- form -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-shadow">New studiant</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->


                        {{ csrf_field() }}


                        <div class="box-body">
                            <div class="premary-content">
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="select">
                                            <div class="form-group">
                                                <div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row content-list">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['name']),['for'=> 'Nom'])}}
                                            {{Form::text('Nom', '', ['class' => 'form-control', 'placeholder' => 'Entrer le Nom de l\'éleve','required'])}}
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['lname']),['for'=> 'Prenom'])}}
                                            {{Form::text('Prenom', '', ['class' => 'form-control', 'placeholder' => 'entrer le Prenom de l\'éleve','required'])}}
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['email']),['for'=> 'Email'])}}
                                            {{Form::email('Email', '', ['class' => 'form-control', 'placeholder' => 'Entrer de Email de l\'éleve','required'])}}
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['birthday']),['for'=> 'birthday'])}}
                                            {{Form::date('birthday', \Carbon\Carbon::now(), ['class' => 'form-control','required'])}}
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['sex']['label']), ['for'=> 'sex'])}}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="radio">
                                                        {{Form::radio('sexe', 1 , true, ['id' => 'sex-homme'])}}
                                                        <span>{{ ucfirst($person_form['sex']['mal']) }}</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="radio">
                                                        {{Form::radio('sexe', 2 , ['id' => 'sex-femme'])}}
                                                        <span>{{ ucfirst($person_form['sex']['femal']) }}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['degree_level']['address']),['for'=> 'Adresse'])}}
                                            {{Form::text('Adresse', '', ['class' => 'form-control', 'placeholder' => 'Entrer de Adresse de l\'éleve','required'])}}
                                        </div>
                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['city']),['for'=> 'Ville'])}}
                                            {{Form::text('Ville', '', ['class' => 'form-control', 'placeholder' => 'Entrer le nom du ville de l\'éleve','required'])}}
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['nationality']),['for'=> 'nationality'])}}
                                            {{Form::text('nationality', '', ['class' => 'form-control', 'placeholder' => 'Entrer la nationalite de l\'éleve','required'])}}
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['degree_level']['postal_code']),['for'=> 'Code Postal'])}}
                                            {{Form::number('Code_Postal', '', ['class' => 'form-control','placeholder' => 'Entrer le code postal de l\'éleve' ,'required'])}}
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('title', ucfirst($person_form['phone_number']),['for'=> 'phone_number'])}}
                                            {{Form::number('phone_number', '', ['class' => 'form-control','placeholder' => 'Entrer le numero de telephone de l\'éleve' ,'required'])}}
                                        </div>

                                    </div>
                                </div>

                                <!--  -->
                            </div>
                        </div>
                    <!-- /.box-body -->


                <!-- end modal-body -->
                </div>
                <div class="modal-footer">

                {{Form::submit('Validate', ['class' =>'btn btn-block btn-lg btn-primary' ])}}
                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
                </div>
            <!-- /form -->
                {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<style>
.radio {
  margin: 16px 0;
  display: block;
  cursor: pointer;
}
.radio input {
  display: none;
}
.radio input + span {
  line-height: 22px;
  height: 22px;
  padding-left: 22px;
  display: block;
  position: relative;
}
.radio input + span:not(:empty) {
  padding-left: 30px;
}
.radio input + span:before, .radio input + span:after {
  content: '';
  width: 22px;
  height: 22px;
  display: block;
  border-radius: 50%;
  left: 0;
  top: 0;
  position: absolute;
}
.radio input + span:before {
  background: #D1D7E3;
  transition: background 0.2s ease, transform 0.4s cubic-bezier(0.175, 0.885, 0.32, 2);
}
.radio input + span:after {
  background: #fff;
  transform: scale(0.78);
  transition: transform 0.6s cubic-bezier(0.175, 0.885, 0.32, 1.4);
}
.radio input:checked + span:before {
  transform: scale(1.04);
  background: #5D9BFB;
}
.radio input:checked + span:after {
  transform: scale(0.4);
  transition: transform 0.3s ease;
}
.radio:hover input + span:before {
  transform: scale(0.92);
}
.radio:hover input + span:after {
  transform: scale(0.74);
}
.radio:hover input:checked + span:after {
  transform: scale(0.4);
}

.radio-inline .radio {
    /* float:left; */
}
.textarea {
    max-width: 100%;
    height: 100%;
    min-height:205px;
}

</style>
