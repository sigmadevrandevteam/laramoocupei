@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

@include('backOffice.users.assignment')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">User</h3>
                </div>
                <div class="box-body">
                    @role('teacher|admin|superadmin')
                    <!-- <h1>Assingation des rôles</h1> -->
                    <h1>Create assets type</h1>
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-12">
                            <form method="post" action="{{ route('superadmin.assets.addtype') }}">
                            @csrf
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" name="name" />
                                </div>
                                <div class="form-group">
                                    <label for="is_active">Enable:</label>
                                    <select class="form-control" name="is_active">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description (optional):</label>
                                    <input type="text" class="form-control" name="description" />
                                </div>
                                <input type="submit" class="btn btn-success" value="Add type" />
                            </form>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            id</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">nom</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Active</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Description</th>
                                        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Actions</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($assets as $key => $asset)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $asset->id }}</td>
                                        <td>{{ $asset->title }}</td>
                                        <td>
                                        {{ $asset->short_description }}
                                        </td>
                                        <td>
                                        {{ $asset->description }}                                        </td>
                                        <td>
                                            <a  href="{{ route('superadmin.assets.deletetype',$asset->id) }}"><button class="btn btn-danger">Delete</button></a>
                                        </td>
                                    </tr>
                                    @endforeach

                                    {{ $users->links() }}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                    <h1>
                        I can't assinging roles
                    </h1>
                    @endrole
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('javascript')

<script type="text/javascript">
    $(document).on('click', '#new-assignment', function(event) {
        event.preventDefault();
        var user_to_assigned = $(this).data('id');
        $('#modal-assignment').modal('show');
        $('#user_to_assigned').val(user_to_assigned);

        $.ajax({
            "url": $(this).data('url'),
            'method': "GET",
            "dataType": "json",
            success: function(user_roles) {

                $('#user_assignment').find('input').each(function($item, $key) {

                    $($key).removeAttr('checked');

                    for (i = 0; i < user_roles.length; i++) {
                        if (user_roles[i] == $($key).data('name')) {

                            $($key).attr('checked', true)
                        }
                    }

                })
                // alert(response)
            }
        });

    });
</script>

@stop