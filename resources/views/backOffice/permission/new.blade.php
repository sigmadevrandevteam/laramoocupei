<!-- /.box-header -->
<div class="modal fade" id="modal-teacher">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{ route('admin.permission.add') }}">
            <!-- form -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-shadow">New permission</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->

                        {{ csrf_field() }}
                        
                        <div class="box-body">
                            <div class="premary-content">
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="content">Name</label>
                                            <input type="" name="name" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <!--  -->
                                <!--  -->
                                

                                <!--  -->
                            </div>
                        </div>
                    <!-- /.box-body -->


                <!-- end modal-body -->
                </div>
                <div class="modal-footer">
                    
                    <button type="submit" class="btn btn-primary" id="">Enregistrer</button>
                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
                </div>
            <!-- /form -->
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{{--
    @section('javascript')

<script>


    $(document).ready(function(){

        //Initialize Select2 Elements
    })
</script>

@stop

--}}